# Changes in srhinow/pin-board-bundle

# 1.3.5 (23.02.2023)
- remove mandatory from 'memberGroupId' in tl_pin_board_setting.php

# 1.3.4 (23.02.2023)
- fix PinBoardSetting::__construct() on install bundle

# 1.3.3 (23.02.2023)
- change composer.lock with lower requires

# 1.3.2 (17.02.2023)
- fixed: deserialize upload field before test is an array

# 1.3.1 (17.02.2023)
- fixed: is upload field not an array
- run codefixer

# 1.3.0 (17.02.2023)
- Changes/fixes only from and up PHP 8.1, Contao 4.13  

# 1.2.0 (17.02.2023)
- add activate/edit/delete-token for public create pinboard-entries
- add Cron to fix expiredDate for older Entries
- add Cron to delete expired entries
- clean code

# 1.1.4 (28.10.2022)
- fix DeleteEntry-Url

# 1.1.3 (28.10.2022)
- fix 'editEntryLinkId' in pbb_entry_list.html5

# 1.1.2 (28.10.2022)
- ModulePinBoardEntryList.php: set url's to current site if its not set.
- fix tl_pin_board_entry.php: drop 'uploaderConfig' from 'upload' eval

# 1.1.1 (27.10.2022)
- change skin on template pbb_new_entry_form.html5 from 'contao' to 'oxide'
- change field-order in tl_module.php

# 1.1.0 (27.10.2022)
- composer.json change to "php": ">=7.4", "terminal42/contao-fineuploader": "^3.3"
- fix strict strlen() param as string

# 1.0.3 (22.03.2022)
- add image - information in front-views

# 1.0.2 (01.01.2022)
- clean Eventlistener PinBoard and PinBoardEntry from copy-code

# 1.0.1 (01.01.2022)
- add README.md with Modul-Description

# 1.0 (01.01.2022)
- create a first functional version