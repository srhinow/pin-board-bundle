<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension pin-board-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

$header = <<<'EOF'
This file is part of the Contao extension pin-board-bundle.

(c) Sven Rhinow (sven@sr-tag.de)

@license LGPL 3.0 or later
EOF;

return (new PhpCsFixer\Config())
    ->setRiskyAllowed(true)
    ->setRules(
        [
            '@Symfony' => true,
            '@Symfony:risky' => true,
            'array_syntax' => ['syntax' => 'short'],
            'combine_consecutive_unsets' => true,
            // one should use PHPUnit methods to set up expected exception instead of annotations
            'general_phpdoc_annotation_remove' => ['annotations' => ['expectedException', 'expectedExceptionMessage', 'expectedExceptionMessageRegExp']],
            'header_comment' => ['header' => $header],
            'heredoc_to_nowdoc' => true,
            'no_extra_blank_lines' => ['tokens' => ['break', 'continue', 'extra', 'return', 'throw', 'use', 'parenthesis_brace_block', 'square_brace_block', 'curly_brace_block']],
            'no_unreachable_default_argument_value' => true,
            'no_useless_else' => true,
            'no_useless_return' => true,
            'ordered_class_elements' => true,
            'ordered_imports' => true,
            'php_unit_strict' => true,
            'phpdoc_add_missing_param_annotation' => true,
            'phpdoc_order' => true,
            'psr_autoloading' => true,
            'strict_comparison' => true,
            'strict_param' => true,
            'native_function_invocation' => ['include' => ['@compiler_optimized']],
            'declare_strict_types' => true,
            'no_superfluous_phpdoc_tags' => true,
            'phpdoc_to_return_type' => true,
            'void_return' => true,
        ]
    )
    ->setFinder(
        PhpCsFixer\Finder::create()
            ->in([__DIR__.'/src'])
    )
    ;
