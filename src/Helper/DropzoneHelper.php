<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension pin-board-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\PinBoardBundle\Helper;

use Contao\Dbafs;
use Contao\File;
use Contao\Files;
use Contao\FilesModel;
use Contao\Folder;
use Contao\FrontendUser;
use Contao\StringUtil;
use Srhinow\PinBoardBundle\EventListener\Dca\PinBoardSetting;

class DropzoneHelper
{
    public $tmpFolder = '';

    public $uploadFolder = '';

    public $extensions = '.jpg,.jpeg,.png,.pdf';

    public $fileSize = 5242880;

    public $fileName = '';

    protected $files;

    protected $pinBoardSettings;

    public function __construct(PinBoardSetting $pinBoardSettings)
    {
        $this->pinBoardSettings = $pinBoardSettings;
        $this->setFromSettings();
    }

    /**
     * nimmt den erlaubten Extension-String auseinander und gibt ihn als Array zurueck.
     */
    public function getExtensionArray(): array
    {
        $array = explode(',', strtolower($this->extensions));

        return \is_array($array) ? $array : [];
    }

    /**
     * bereinigt Dateinamen.
     */
    public function cleanFileName(string $name): string
    {
        $name = StringUtil::sanitizeFileName($name);

        return str_replace(' ', '_', $name);
    }

    /**
     * prüft die Datei-Erweiterung.
     */
    public function checkExtensions(array $file): bool
    {
        $path_parts = pathinfo($file['name']);

        // Wenn keine Dateierweiterung uebrgeben wurde -> abbrechen
        if ('' === $path_parts['extension']) {
            return false;
        }

        // Wenn die Datei-Erweiterung nicht erlaubt ist -> abbrechen
        if (!\in_array('.'.strtolower($path_parts['extension']), $this->getExtensionArray(), true)) {
            return false;
        }

        // wenn alles passt
        return true;
    }

    /**
     * prüft die Dateigröße.
     */
    public function checkFileSize(array $file): bool
    {
        $file['size'] = (int) $file['size'];

        // wenn die Dateigroeße kleiner als 1 byte ist stimmt was nicht
        if ($file['size'] < 1) {
            return false;
        }

        // wenn die Dateigroesse mehr als erlaubt ist
        if ($file['size'] > ceil($this->fileSize * 1024 * 1024)) {
            return false;
        }

        // wenn alles passt
        return true;
    }

    public function setAjaxResponse(string $status = 'error', string $message = '', array $array2 = []): void
    {
        echo json_encode(
            array_merge(
                [
                    'status' => $status,
                    'message' => $message,
                ],
                $array2
            )
        );
        exit;
    }

    /**
     * per Ajax Datei im System ablegen.
     *
     * @param null $files
     *
     * @throws \Exception
     */
    public function upload($files = null): bool
    {
        $this->files = (null === $files) ? $_FILES : $files;
        if (!\is_array($this->files)) {
            $this->setAjaxResponse('error', 'no_files');
        }

        $arrFiles = [];

        foreach ($this->files as $file) {
            // auf passende Datei-Erweiterung pruefen
            if (!$this->checkExtensions($file)) {
                $this->setAjaxResponse(
                    'error',
                    'extension',
                    ['file' => $file]
                );
            }

            // auf Dateigröße prüfen
            if (!$this->checkFileSize($file)) {
                $this->setAjaxResponse(
                    'error',
                    'filesize',
                    ['file' => $file]
                );
            }

            // Dateiname bereinigen
            $this->fileName = $this->cleanFileName($file['name']);

            // Ziel-Pfad mit FrontendUser-ID als Unterverzeichnis
            $targetPath = $this->uploadFolder.'/'.$this->getSubFolder();
            if (!file_exists($targetPath)) {
                new Folder($targetPath);
                \Dbafs::addResource($targetPath);
            }

            $targetPathAbs = $targetPath.'/'.$this->fileName;

            // Datei in Ziel-Pfad kopieren
            $Files = Files::getInstance();
            if (!$Files->move_uploaded_file($file['tmp_name'], $targetPathAbs)) {
                $this->setAjaxResponse(
                    'error',
                    'move_uploaded_file',
                    ['file' => $targetPathAbs]
                );
            }

            // Berechtigung setzen
            $Files->chmod($targetPathAbs, 0666 & ~umask());

            // pruefen, ob die Datei dort existiert, wo sie hinkopiert wurde
            if (!file_exists(TL_ROOT.'/'.$targetPathAbs)) {
                $this->setAjaxResponse(
                    'error',
                    'file_not_exist',
                    ['file' => $targetPathAbs]
                );
            }

            // in Contao-Files-Logik aufnehmen
            Dbafs::addResource($targetPathAbs);

            $arrFiles[] = $this->fileName;
        }

        // nach dem Hochladen aller Files ohne Fehler → success
        $this->setAjaxResponse(
            'success',
            'files_uploadet',
            ['files' => $arrFiles]
        );

        return true;
    }

    public function remove($fileName): void
    {
        // Dateiname bereinigen und auf den gleichen Stand bringen wie im System abgelegt
        $this->fileName = $this->cleanFileName($fileName);

        $filePath = $this->uploadFolder.'/'.$this->getSubFolder().'/'.$this->fileName;

        // prüfen, ob die Datei dort existiert, wo sie hinkopiert wurde
        if (!file_exists(TL_ROOT.'/'.$filePath)) {
            $this->setAjaxResponse(
                'error',
                'file_not_exist',
                ['path' => $filePath, 'file' => $this->fileName]
            );
        }

        // prüfen, ob die der Pfad ein Ordner und keine Datei ist
        if (is_dir(TL_ROOT.'/'.$filePath)) {
            $this->setAjaxResponse(
                'error',
                'is_a_dir',
                ['path' => $filePath, 'file' => $this->fileName]
            );
        }

        try {
            (new File($filePath))->delete();
        } catch (\Exception $e) {
            $this->setAjaxResponse(
                'error',
                'delete_failed',
                ['path' => $filePath, 'file' => $this->fileName]
            );
        }
        $this->setAjaxResponse(
            'success',
            'file_deleted',
            ['path' => $filePath, 'file' => $this->fileName]
        );
    }

    public function getSubFolder(): string
    {
        $FeUser = FrontendUser::getInstance();
        $subFolder = $FeUser->id ?? 'public';

        return (string) $subFolder;
    }

    /**
     * überschreibt / setzt die Parameter mit den aus den Pin Board-Einstellungen.
     */
    protected function setFromSettings(): void
    {
        $objSettings = $this->pinBoardSettings->getObjSettings();
        $this->uploadFolder = FilesModel::findByUuid($objSettings->uploadFolder)->path;
        $this->extensions = $objSettings->uploadFileExtensions;
        $this->fileSize = $objSettings->uploadFileSize;
    }
}
