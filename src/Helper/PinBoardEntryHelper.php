<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension pin-board-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\PinBoardBundle\Helper;

use Contao\Controller;
use Contao\CoreBundle\Monolog\ContaoContext;
use Contao\Dbafs;
use Contao\Files;
use Contao\FilesModel;
use Contao\Folder;
use Contao\Frontend;
use Contao\FrontendUser;
use Contao\Input;
use Contao\PageModel;
use Contao\StringUtil;
use Contao\System;
use Psr\Log\LogLevel;
use Srhinow\PinBoardBundle\EventListener\Dca\PinBoardSetting;
use Srhinow\PinBoardBundle\Model\PinBoardEntryModel;
use Srhinow\PinBoardBundle\Model\PinBoardModel;
use Symfony\Bridge\Monolog\Logger;

class PinBoardEntryHelper
{
    protected $helper;

    protected $dropzoneHelper;

    protected $pinBoardSetting;

    protected $logger;

    public function __construct(
        Helper $helper,
        DropzoneHelper $dropzoneHelper,
        PinBoardSetting $pinBoardSetting,
        Logger $logger)
    {
        $this->helper = $helper;
        $this->dropzoneHelper = $dropzoneHelper;
        $this->pinBoardSetting = $pinBoardSetting;
        $this->logger = $logger;
    }

    /**
     * speichert den Nachrichten-Entwurf.
     */
    public function saveEntryFormActions(): ?PinBoardEntryModel
    {
        if (null === ($btnSend = Input::post('send'))) {
            return null;
        }

        $dCounts = (int) Input::post('dokumente');
        $Member = FrontendUser::getInstance();
        $EntryId = (int) Input::post('entryId');

        if (1 > $EntryId) {
            $objPinBoardEntry = new PinBoardEntryModel();
            $objPinBoardEntry->pid = Input::post('pinBoardId');
            $objPinBoardEntry->dateAdded = time();
            $objPinBoardEntry->published = '';
            $objPinBoardEntry->new = '1';
            $objPinBoardEntry->activateToken = Helper::uniqueReal(48);
            $objPinBoardEntry->editToken = Helper::uniqueReal(48);
            $objPinBoardEntry->deleteToken = Helper::uniqueReal(48);
            if (isset($Member->id)) {
                $objPinBoardEntry->memberId = $Member->id;
                $objPinBoardEntry->authorName = $Member->username;
            }
        } else {
            if (null === ($objPinBoardEntry = PinBoardEntryModel::findOneBy(['`id`=?'], [$EntryId]))) {
                return null;
            }
        }

        $objPinBoardEntry->tstamp = time();
        $objPinBoardEntry->modify = time();
        // Bei jedem speichern/editieren wird das Ablaufdatum eu gesetzt.
        $objPinBoardEntry->dateExpired = strtotime('+'.$this->pinBoardSetting->getObjSettings()->closedAfterDays.' days');
        $objPinBoardEntry->subject = trim(Input::post('subject'));
        $objPinBoardEntry->email = trim(Input::post('email'));
        $objPinBoardEntry->text = html_entity_decode(trim(Input::post('message')));
        $objPinBoardEntry->save();

        // Uploads
        $jsonAttachFiles = Input::post('attachfiles');
        $arrAttachFiles = ($jsonAttachFiles) ? json_decode(html_entity_decode($jsonAttachFiles), true) : [];
        if ($dCounts > 0 && \is_array($arrAttachFiles) && \count($arrAttachFiles) > 0) {
            try {
                $objPinBoardEntry->upload = $this->convertFileArrayForDatabase(
                    $this->moveUploadsToPinBoardFolder($arrAttachFiles, $objPinBoardEntry->id)
                );
            } catch (\Exception $e) {
            }
        }

        $objPinBoardEntry->save();

        return $objPinBoardEntry;
    }

    public function sendEntryNotification(int $notificationId, $entryId = null, $arrToken = []): void
    {
        if ($notificationId < 1) {
            return;
        }

        // Notification nur einmal senden
        if (null !== $entryId && null !== ($objEntry = PinBoardEntryModel::findByPk($entryId))) {
            $arrToken['entry_owner_email'] = $objEntry->email;
            $arrToken['activate_token'] = $objEntry->activateToken;
            $arrToken['edit_token'] = $objEntry->editToken;
            $arrToken['delete_token'] = $objEntry->deleteToken;
        }

        $this->helper->sendNotification($notificationId, $arrToken);
    }

    public function generateFrontendUrlWithParameter(int $pageId, string $parameter)
    {
        $strHref = $_SERVER['REQUEST_URI'];
        $objPage = PageModel::findByPk($pageId);
        if (null !== $objPage) {
            $objUrlGenerator = System::getContainer()->get('contao.routing.url_generator');

            $strUrl = $objUrlGenerator->generate(
                ($objPage->alias ?: $objPage->id).'/'.$parameter,
                [
                    '_locale' => $objPage->rootLanguage,
                    '_domain' => $objPage->domain,
                    '_ssl' => (bool) $objPage->rootUseSSL,
                ],
                $objUrlGenerator::ABSOLUTE_URL
            );
            $strHref = StringUtil::ampersand($strUrl);
        }

        return $strHref;
    }

    /**
     * @return array|bool
     */
    public function getLinksFromUpload(?string $uploadField)
    {
        $arrFiles = [];

        if (null === $uploadField) {
            return $arrFiles;
        }

        $arrUploads = \is_array($uploadField) ? $uploadField : unserialize($uploadField);

        if (!\is_array($arrUploads) || \count($arrUploads) < 1) {
            return $arrFiles;
        }

        foreach ($arrUploads as $upload) {
            if (!\strlen($upload)) {
                continue;
            }

            $objFile = \FilesModel::findByUuid($upload);

            if (null === $objFile) {
                continue;
            }

            $arrFiles[] = [
                'showFile' => Frontend::addToUrl('do=showfile&file='.$objFile->id),
                'name' => $objFile->name,
                'extension' => $objFile->extension,
                'path' => $objFile->path,
            ];
        }

        return (\count($arrFiles) < 0) ? false : $arrFiles;
    }

    /**
     * @return array|bool
     */
    public function getFileInfosFromUploadField(?string $uploadField)
    {
        $arrFiles = [];

        if (null === $uploadField) {
            return $arrFiles;
        }

        $uploadField = StringUtil::deserialize($uploadField);
        $arrUploads = \is_array($uploadField) ? $uploadField : [$uploadField];

        if (\is_array($arrUploads) && \count($arrUploads) > 0) {
            foreach ($arrUploads as $ufile) {
                if (null === ($file = FilesModel::findByUuid($ufile))) {
                    continue;
                }

                $obj['name'] = $file->name;
                $obj['size'] = filesize(TL_ROOT.'/'.$file->path);
                $obj['path'] = $file->path;
                $arrFiles[] = $obj;
            }
        }

        return (\count($arrFiles) < 0) ? false : $arrFiles;
    }

    public function getPinBoardEntryFolder($entryId): string
    {
        $entryFolder = 'files'.\DIRECTORY_SEPARATOR.$entryId;

        if (null === ($objPinBoardEntry = PinBoardEntryModel::findByPk($entryId))) {
            return $entryFolder;
        }
        if (null === ($objPinBoard = PinBoardModel::findByPk($objPinBoardEntry->pid))) {
            return $entryFolder;
        }
        $entryFolder = $objPinBoard->pinBoardFolder.\DIRECTORY_SEPARATOR.$entryId;

        $this->createEntryFolder($entryFolder);

        return $entryFolder;
    }

    /**
     * prüft/erstellt den Pfad zum Pinnwand-Eintrag.
     *
     * @throws \Exception
     */
    public function createEntryFolder($pinBoardEntryPath = ''): bool
    {
        // falls nicht vorhanden den Speicherort-Ordner anlegen
        if (!file_exists(TL_ROOT.'/'.$pinBoardEntryPath)) {
            if (!mkdir(TL_ROOT.'/'.$pinBoardEntryPath)) {
                return false;
            }

            \Dbafs::addResource($pinBoardEntryPath);
        }

        return true;
    }

    public function dropPinBoardEntryFolder($entryId): void
    {
        try {
            $objFolder = new Folder($this->getPinBoardEntryFolder($entryId));
            $objFolder->purge();
            $objFolder->delete();

            // Add a log entry
            $this->logger->log(
                LogLevel::INFO,
                'Purged the PinBoard Upload-Folder: '.$objFolder->path,
                ['contao' => new ContaoContext(__METHOD__, TL_GENERAL)]
            );
        } catch (\Exception $e) {
            // Add a log entry
            $this->logger->log(
                LogLevel::ERROR,
                'NO Purged the PinBoard Upload-Folder: '.$e->getMessage(),
                ['contao' => new ContaoContext(__METHOD__, TL_ERROR)]
            );
        }
    }

    /**
     * verschiebt die per DropZone hochgeladene Datei aus den upload-Folder in den passenden PinBoard-Ordner.
     *
     *@throws \Exception
     */
    public function moveUploadsToPinBoardFolder(array $arrFiles = [], $PinBoardEntryId = '0'): array
    {
        $arrReturn = [];
        if (!\is_array($arrFiles) || \count($arrFiles) < 1) {
            return $arrReturn;
        }

        $pinBoardId = (int) Input::post('pinBoardId');
        if (null === ($objPinBoard = PinBoardModel::findByPk($pinBoardId))) {
            return $arrReturn;
        }

        $uploadPath = $this->dropzoneHelper->uploadFolder.\DIRECTORY_SEPARATOR.$this->dropzoneHelper->getSubFolder();
        $EntryFolderPath = $this->getPinBoardEntryFolder($PinBoardEntryId);
        $Files = Files::getInstance();

        foreach ($arrFiles as $file) {
            $srcFile = $uploadPath.\DIRECTORY_SEPARATOR.$file;
            $dstFile = $EntryFolderPath.\DIRECTORY_SEPARATOR.$file;
            if (!$Files->rename($srcFile, $dstFile)) {
                continue;
            }

            // in Contao-Files-Logik aufnehmen
            Dbafs::addResource($dstFile);

            $arrReturn[] = $dstFile;
        }

        return $arrReturn;
    }

    /**
     * Konvertiert die Dateipfade in uuid's für die upload-Datenbankspalte.
     */
    public function convertFileArrayForDatabase(array $arrFiles = []): array
    {
        $arrReturn = [];
        if (!\is_array($arrFiles) || \count($arrFiles) < 1) {
            return $arrReturn;
        }

        foreach ($arrFiles as $FilePath) {
            $objFile = FilesModel::findByPath($FilePath);
            if (null === $objFile) {
                continue;
            }

            $arrReturn[] = $objFile->uuid;
        }

        return $arrReturn;
    }

    /**
     * Konvertiert die Uuid's in Dateipfade für die upload-Datenbankspalte.
     */
    public function convertFileArrayForJson(array $arrFiles = []): array
    {
        $arrReturn = [];
        if (!\is_array($arrFiles) || \count($arrFiles) < 1) {
            return $arrReturn;
        }

        foreach ($arrFiles as $uuid) {
            $objFile = FilesModel::findByUuid($uuid);
            if (null === $objFile) {
                continue;
            }

            $arrReturn[] = basename($objFile->path);
        }

        return $arrReturn;
    }

    public function setRecordAsRead($recordId = null): void
    {
        if (null === $recordId || (int) $recordId < 1) {
            $response = ['status' => 'error', 'msg' => 'recordId "'.$recordId.'" stimmt nicht.'];
            echo json_encode($response);
            exit;
        }

        $objRecord = PinBoardEntryModel::findByPk((int) $recordId);
        if (null === $objRecord) {
            $response = ['status' => 'error', 'msg' => 'kein Record-Datensatz zu "'.$recordId.'" gefunden.'];
            echo json_encode($response);
            exit;
        }

        $objRecord->dateOpen = time();
        $objRecord->new = '';
        $objRecord->save();

        $response = ['status' => 'success', 'msg' => 'Record "'.$recordId.'" als gelesen markieren.'];
        echo json_encode($response);
        exit;
    }

    /**
     * wenn GET-Parameter passen dann wird eine Datei zum Browser gesendet.
     */
    public function showDocumentForMember(): void
    {
        if ('showfile' !== Input::get('do') || !$id = Input::get('file')) {
            return;
        }

        if (null === ($objFile = FilesModel::findByPk($id))) {
            return;
        }

        Controller::sendFileToBrowser($objFile->path);
    }
}
