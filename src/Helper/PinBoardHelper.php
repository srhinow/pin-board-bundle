<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension pin-board-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\PinBoardBundle\Helper;

use Contao\CoreBundle\Monolog\ContaoContext;
use Contao\Folder;
use Psr\Log\LogLevel;
use Srhinow\PinBoardBundle\Model\PinBoardModel;
use Symfony\Bridge\Monolog\Logger;

class PinBoardHelper
{
    /**
     * @var Logger
     */
    protected $logger;

    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function createFolderIfNotExist($folderPath): bool
    {
        // falls nicht vorhanden den Speicherort-Ordner anlegen
        if (!file_exists(TL_ROOT.\DIRECTORY_SEPARATOR.$folderPath)) {
            if (!mkdir(TL_ROOT.\DIRECTORY_SEPARATOR.$folderPath, 0755, true)) {
                $this->logger->log(LogLevel::INFO, sprintf('Folder "%s" existiert nicht.', TL_ROOT.'/'.$folderPath));

                return false;
            }

            return true;
        }

        return true;
    }

    public function dropPinBoardFolder($entryId): void
    {
        if (null === ($objPinBoard = PinBoardModel::findByPk($entryId))) {
            return;
        }

        try {
            $uploadPath = $objPinBoard->pinBoardFolder;
            $objFolder = new Folder($uploadPath);

            $objFolder->purge();
            $objFolder->delete();

            // Add a log entry
            $this->logger->log(
                LogLevel::INFO,
                'Purged the PinBoard Upload-Folder',
                ['contao' => new ContaoContext(__METHOD__, TL_GENERAL)]
            );
        } catch (\Exception $e) {
            // Add a log entry
            $this->logger->log(
                LogLevel::ERROR,
                'NO Purged the PinBoard Upload-Folder:'.$uploadPath,
                ['contao' => new ContaoContext(__METHOD__, TL_ERROR)]
            );
        }
    }
}
