<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension pin-board-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\PinBoardBundle\Model;

/*
 * @author     Sven Rhinow
 * @package    srhinow/lawyer-client-portal
 * @filesource
 */

use Contao\Model;
use Contao\Model\Collection;

use function count;

class PinBoardEntryModel extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected static $strTable = 'tl_pin_board_entry';

    /**
     * sucht alle, definierten Pin-Board Einträge.
     *
     * @return Model|Model[]|Model\Collection|PinBoardEntryModel|int|null
     */
    public static function findPublishedPinBoardEntries(
        array $columns = [],
        string $search = '',
        int $memberId = null,
        array $arrOptions = [],
        $dbType = 'find' // find | count
    ) {
        $t = static::$strTable;

        $arrValues = null;

        // nach aktuellem Mitglied filtern
        if (null !== $memberId) {
            $arrColumns[] = "$t.`memberId`=".$memberId;
        }

        // nur nicht gelöschte nachrichten
        $arrColumns[] = "$t.`published` = '1'";

        // Wenn weitere Filter übergeben wurden z.B. case=1
        if (\is_array($columns) && (\count($columns) > 0)) {
            foreach ($columns as $ck => $cv) {
                $arrColumns[] = "$t.`$ck`=".$cv;
            }
        }

        // wenn was im Filter-Suchfeld übergeben wurde
        if ('' !== $search) {
            $arrFields = ['subject', 'text', 'authorName'];
            $search = strtolower(trim($search));
            $orColumns = [];

            foreach ($arrFields as $field) {
                $orColumns[] = "LOWER($t.`$field`) LIKE ?";
                $arrValues[] = '%'.$search.'%';
            }

            if (\count($orColumns) > 0) {
                $arrColumns[] = '('.implode(' OR ', $orColumns).')';
            }
        }

        if (!isset($arrOptions['order'])) {
            $arrOptions['order'] = "$t.dateAdded DESC";
        }

        if ('count' === $dbType) {
            return static::countBy($arrColumns, $arrValues, $arrOptions);
        }

        return static::findBy($arrColumns, $arrValues, $arrOptions);
    }

    /**
     * Count all published entries to pin board.
     *
     * @param array $arrOptions An optional options array
     *
     * @return int The number of all new Case Records
     */
    public static function countPublishedPinBoardEntries(
        array $columns = [],
        string $search = '',
        int $memberId = null,
        array $arrOptions = []
    ): int {
        return self::findPublishedPinBoardEntries($columns, $search, $memberId, $arrOptions, 'count');
    }

    /**
     * @param string $tokenType // edit | delete | activate
     *
     * @return Collection|PinBoardEntryModel|null
     */
    public static function findEntryByToken(string $token = '', string $tokenType = 'edit')
    {
        if (empty($token)) {
            return null;
        }

        $t = static::$strTable;

        // nach aktuellem Mitglied filtern
        $fieldName = $tokenType.'Token'; // editToken
        $arrColumns[] = "$t.$fieldName=".$token;

        return static::findOneBy($arrColumns, null);
    }
}
