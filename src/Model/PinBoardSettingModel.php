<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension pin-board-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\PinBoardBundle\Model;

/*
 * @author     Sven Rhinow
 * @package    srhinow/lawyer-client-portal
 * @filesource
 */

use Contao\Model;

class PinBoardSettingModel extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected static $strTable = 'tl_pin_board_setting';
}
