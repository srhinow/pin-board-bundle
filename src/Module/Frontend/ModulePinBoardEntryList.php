<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension pin-board-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\PinBoardBundle\Module\Frontend;

use Contao\BackendTemplate;
use Contao\Database;
use Contao\FrontendUser;
use Contao\Input;
use Contao\System;
use Srhinow\PinBoardBundle\Helper\MemberHelper;
use Srhinow\PinBoardBundle\Model\PinBoardEntryModel;
use Srhinow\PinBoardBundle\Model\PinBoardModel;

class ModulePinBoardEntryList extends ModulePinBoardFrontend
{
    /**
     * Template.
     *
     * @var string
     */
    protected $strTemplate = 'pbb_entry_list';

    protected static string $COOKIE_NAME_MESSAGE = 'PIN_BOARD_MESSAGE';
    protected static string $COOKIE_ALERT_TYPE = 'PIN_BOARD_ALERT_TYPE';

    /**
     * Target pages.
     */
    protected array $arrTargets = [];

    /**
     * Display a wildcard in the back end.
     *
     * @throws \Exception
     */
    public function generate(): string
    {
        if (TL_MODE === 'BE') {
            $objTemplate = new BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### PIN BOARD EINTRAG LISTE ###';

            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id='.$this->id;

            return $objTemplate->parse();
        }

        // Fallback template
        if ($this->pbb_fe_template) {
            $this->strTemplate = $this->pbb_fe_template;
        }

        // Set the item from the auto_item parameter
        if ($GLOBALS['TL_CONFIG']['useAutoItem'] && \Input::get('auto_item')) {
            \Input::setGet('pid', \Input::get('auto_item'));
        }

        return parent::generate();
    }

    /**
     * Generate module.
     */
    protected function compile(): void
    {
        // prüfen und den zugehörenden PinBoard Datensatz holen
        if (null === $this->pbb_pinBoard || null === ($objPinBoard = PinBoardModel::findByPk($this->pbb_pinBoard))) {
            return;
        }

        // prüfen, wenn es nur im geschützten Bereich angezeigt werden soll
        if ($this->protected) {
            // raus, wenn der Nutzer nicht eingeloggt ist
            if (!FE_USER_LOGGED_IN) {
                return;
            }

            // prüfen, ob der aktuelle Nutzer zur, im Modul eingestellten Gruppen, zugelassenen Mitgliedergruppe gehört
            $Member = FrontendUser::getInstance();
            if (!MemberHelper::isMemberOf($this->groups, $Member)) {
                return;
            }
        }

        $EntryHelper = System::getContainer()->get('srhinow.pin_board_bundle.helper.pin_board_entry_helper');
        $this->loadLanguageFile('tl_pin_board_entry');
        $arrItems = [];
        $dbSearch = Input::post('search') ? htmlspecialchars(Input::post('search')) : '';

        if ('showfile' === Input::get('do')) {
            $EntryHelper->showDocumentForMember();
        }

        // Falls der Eintrag gelöscht werden soll und der aktuelle Member auch der Author ist
        if ('del' === Input::get('do') && null !== Input::get('entry')) {
            if (null === ($objDelEntry = PinBoardEntryModel::findOneBy(
                ['memberId=?', 'id=?'],
                [$Member->id, (int) Input::get('entry')]
            ))) {
                // @ToDo eventuell noch als Log-Eintrag hinterlegen
                return;
            }
            $objDelEntry->delete();
        }

        $columns = [
            'pid' => $this->pbb_pinBoard,
        ];

        // je nachdem ob alle oder nur des aktuellen Members gezählt werden sollen
        if ($this->protected && '' !== $this->onlyMemberEntries) {
            $this->total = PinBoardEntryModel::countPublishedPinBoardEntries(
                $columns,
                $dbSearch,
                $Member->id
            );
        } else {
            $this->total = PinBoardEntryModel::countPublishedPinBoardEntries(
                $columns,
                $dbSearch
            );
        }

        if ($this->total > 0) {
            $this->setListPagination();

            // falls eine Sortierung übergeben wurde diese setzen
            $options = [
                'limit' => $this->limit,
                'offset' => $this->offset,
                'order' => 'dateAdded DESC',
            ];

            $sorting = Input::post('sorting');
            if (isset($sorting) && '' !== $sorting) {
                $options['order'] = $sorting;
            }

            // je nachdem ob alle oder nur des aktuellen Members geholt werden sollen
            if ($this->protected && '' !== $this->onlyMemberEntries) {
                $objEntries = PinBoardEntryModel::findPublishedPinBoardEntries(
                    $columns,
                    $dbSearch,
                    (int) $Member->id,
                    $options
                );
            } else {
                $objEntries = PinBoardEntryModel::findPublishedPinBoardEntries(
                    $columns,
                    $dbSearch,
                    null,
                    $options
                );
            }
            if (null !== $objEntries) {
                while ($objEntries->next()) {
                    // Author des Eintrages holen
                    $arrAuthor = [];
                    $dbMember = Database::getInstance()
                        ->prepare('SELECT firstname,lastname,email,username FROM tl_member WHERE id=?')
                        ->limit(1)
                        ->execute($objEntries->memberId);
                    if ($dbMember->numRows > 0) {
                        $arrAuthor = $dbMember->fetchAssoc();
                    }

                    $strDetailHref = $EntryHelper->generateFrontendUrlWithParameter((int) $this->jumpToEntryDetails, (string) $objEntries->id);

                    // uploads
                    $arrFileLinks = $EntryHelper->getLinksFromUpload($objEntries->upload);
                    $arrFileInfos = $EntryHelper->getFileInfosFromUploadField($objEntries->upload);

                    // PinBoard-Eigenschaften zusammenstellen
                    $arrItems[] = [
                        'entryId' => $objEntries->id,
                        'pinBoardId' => $objPinBoard->id,
                        'pinBoardTitle' => $objPinBoard->title,
                        'isMyEntry' => ($this->protected && $objEntries->memberId === $Member->id),
                        'author' => $arrAuthor,
                        'dateAdded' => $objEntries->dateAdded,
                        'boolFileLinks' => $arrFileLinks && \count($arrFileLinks) > 0,
                        'fileLinks' => $arrFileLinks,
                        'boolFileInfos' => $arrFileInfos && \count($arrFileInfos) > 0,
                        'fileInfos' => $arrFileInfos,
                        'subject' => $objEntries->subject,
                        'new' => $objEntries->new,
                        'text' => $objEntries->text,
                        'detailHref' => $strDetailHref,
                    ];
                }
            } else {
                $this->Template->message = $this->noEntryContent;
                $this->Template->alertType = 'danger';
            }
        } else {
            $this->Template->message = $this->noEntryContent;
        }

        // wenn es vorher schon, durch eventuelle Fehler abgebrochen wurde,
        // werden alle Inhalte in dem bereich nicht angezeigt.
        $this->Template->showListContent = true;
        $this->Template->deleteEntryLinkId = $GLOBALS['objPage']->id;
        $this->Template->newEntryLinkId = ($this->jumpToNewEntryForm > 0) ? $this->jumpToNewEntryForm : $GLOBALS['objPage']->id;
        $this->Template->editEntryLinkId = ($this->jumpToEditEntryForm > 0) ? $this->jumpToEditEntryForm : $GLOBALS['objPage']->id;
        $this->Template->items = $arrItems;
        $this->Template->total = $this->total;
        $this->Template->search = $dbSearch;
    }
}
