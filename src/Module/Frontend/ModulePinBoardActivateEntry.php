<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension pin-board-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\PinBoardBundle\Module\Frontend;

use Contao\BackendTemplate;
use Contao\Input;
use Srhinow\PinBoardBundle\Model\PinBoardEntryModel;

class ModulePinBoardActivateEntry extends ModulePinBoardFrontend
{
    /**
     * Template.
     *
     * @var string
     */
    protected $strTemplate = 'pbb_activate_entry';

    /**
     * Display a wildcard in the back end.
     *
     * @throws \Exception
     */
    public function generate(): string
    {
        if (TL_MODE === 'BE') {
            $objTemplate = new BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### PIN BOARD EINTRAG AKTIVIEREN ###';

            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id='.$this->id;

            return $objTemplate->parse();
        }

        // Fallback template
        if ($this->pbb_fe_template) {
            $this->strTemplate = $this->pbb_fe_template;
        }

        // Set the item from the auto_item parameter
        if ($GLOBALS['TL_CONFIG']['useAutoItem'] && \Input::get('auto_item')) {
            \Input::setGet('token', \Input::get('auto_item'));
        }

        return parent::generate();
    }

    /**
     * Generate module.
     */
    protected function compile(): void
    {
        $token = Input::get('token');
        if (!isset($token)) {
            $this->Template->text = $this->noEntryForToken;
            $this->Template->ready = false;

            return;
        }

        if (null === ($objEntry = PinBoardEntryModel::findOneBy(['activateToken =?'], [$token]))) {
            $this->Template->text = $this->noEntryForToken;
            $this->Template->ready = false;

            return;
        }

        $objEntry->published = 1;
        $objEntry->save();

        $this->Template->text = $this->successfullyActionForToken;
        $this->Template->ready = true;
    }
}
