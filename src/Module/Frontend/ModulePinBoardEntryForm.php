<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension pin-board-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\PinBoardBundle\Module\Frontend;

use Contao\BackendTemplate;
use Contao\Controller;
use Contao\Input;
use Contao\PageModel;
use Contao\System;
use Srhinow\PinBoardBundle\Model\PinBoardEntryModel;

class ModulePinBoardEntryForm extends ModulePinBoardFrontend
{
    /**
     * Template.
     *
     * @var string
     */
    protected $strTemplate = 'pbb_entry_list';

    protected static string $COOKIE_NAME_MESSAGE = 'PIN_BOARD_MESSAGE';
    protected static string $COOKIE_ALERT_TYPE = 'PIN_BOARD_ALERT_TYPE';

    /**
     * Target pages.
     */
    protected array $arrTargets = [];

    /**
     * Display a wildcard in the back end.
     *
     * @throws \Exception
     */
    public function generate(): string
    {
        if (TL_MODE === 'BE') {
            $objTemplate = new BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### PIN BOARD EINTRAG FORMULAR ###';

            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id='.$this->id;

            return $objTemplate->parse();
        }

        // Ajax Requests abfangen
        if ($this->Environment->get('isAjaxRequest')) {
            $DropZoneHelper = System::getContainer()->get('srhinow.pin_board_bundle.helper.dropzone_helper');
            if (!empty(Input::get('del'))) {
                $DropZoneHelper->remove(Input::get('del'));
            } elseif (\is_array($_FILES) && \count($_FILES) > 0) {
                $DropZoneHelper->upload($_FILES);
            }

            exit;
        }

        // Fallback template
        if ($this->pbb_fe_template) {
            $this->strTemplate = $this->pbb_fe_template;
        }

        // Set the item from the auto_item parameter
        if ($GLOBALS['TL_CONFIG']['useAutoItem'] && \Input::get('auto_item')) {
            \Input::setGet('entry', \Input::get('auto_item'));
        }

        return parent::generate();
    }

    /**
     * Generate module.
     */
    protected function compile(): void
    {
        $EntryHelper = System::getContainer()->get('srhinow.pin_board_bundle.helper.pin_board_entry_helper');
        $pinBoardSettings = System::getContainer()->get('srhinow.pin_board_bundle.listeners.dca.pin_board_setting');
        $objSettings = $pinBoardSettings->getObjSettings();
        $this->loadLanguageFile('tl_pin_board_entry');
        $arrItems = [];
        $entryFormId = 'new_entry_form_'.$this->id;

        if ('showfile' === Input::get('do')) {
            $EntryHelper->showDocumentForMember();
        }

        // CasePage-Url generieren
        $objJumpToPage = PageModel::findByPk($this->jumpTo);
        $jumpToUrl = (null === $objJumpToPage) ? $_SERVER['REQUEST_URI'] : $objJumpToPage->getFrontendUrl();

        // verarbeiten von "neue Nachrichten" - Events
        if ($entryFormId === Input::post('FORM_SUBMIT') && '' === Input::post('company')) {
            // Nachricht senden
            if (null !== Input::post('send')) {
                if (null !== ($objEditEntry = $EntryHelper->saveEntryFormActions())) {
                    setcookie(self::$COOKIE_NAME_MESSAGE, 'Der Eintrag wurde gespeichert.', time() + 1000000);
                    setcookie(self::$COOKIE_ALERT_TYPE, 'success', time() + 1000000);

                    if ((int) $this->notificationId > 0) {
                        $arrToken = [];
                        if ($this->editEntryPage > 0 && $objEditEntry->editToken) {
                            // Redirect to the jumpTo page
                            $arrToken['edit_link'] = $EntryHelper->generateFrontendUrlWithParameter((int) $this->editEntryPage, (string) $objEditEntry->editToken);
                        }
                        if ($this->activateEntryPage > 0 && $objEditEntry->activateToken) {
                            // Redirect to the jumpTo page
                            $arrToken['activate_link'] = $EntryHelper->generateFrontendUrlWithParameter((int) $this->activateEntryPage, (string) $objEditEntry->activateToken);
                        }
                        if ($this->deleteEntryPage > 0 && $objEditEntry->deleteToken) {
                            // Redirect to the jumpTo page
                            $arrToken['delete_link'] = $EntryHelper->generateFrontendUrlWithParameter((int) $this->deleteEntryPage, (string) $objEditEntry->deleteToken);
                        }
                        $EntryHelper->sendEntryNotification((int) $this->notificationId, $objEditEntry->id, $arrToken);
                    }
                } else {
                    setcookie(
                        self::$COOKIE_NAME_MESSAGE,
                        'Es ist ein Fehler aufgetreten. Die Nachricht wurde nicht gesendet.',
                        time() + 1000000
                    );
                    setcookie(self::$COOKIE_ALERT_TYPE, 'danger', time() + 1000000);
                }
                Controller::redirect($jumpToUrl);
            }
        }

        $arrForm['pinBoard'] = $this->pbb_pinBoard;

        // das Formular auch als Edit-Formular verwenden
        $objEntry = null;

        // nur wenn der Eintrag (Edit-Token) als GET-Parameter gesetzt wurde und existiert
        $getEntry = Input::get('entry');
        if (isset($getEntry)) {
            $objEntry = PinBoardEntryModel::findOneBy(
                ['editToken=?'],
                [$getEntry]
            );
        }

        if (null !== $objEntry) {
            $arrForm['email'] = $objEntry->email;
            $arrForm['subject'] = $objEntry->subject;
            $arrForm['message'] = html_entity_decode($objEntry->text);

            $this->Template->entryId = $objEntry->id;
            $this->Template->editEntry = !(null === $objEntry);
        }

        $this->Template->pinBoardId = $this->pbb_pinBoard;
        $this->Template->entryFormId = $entryFormId;
        $this->Template->form = $arrForm;
        $this->Template->total = $this->total;
        $this->Template->addUpload = $this->addUpload;
        $this->Template->beforeUploadContent = $this->beforeUploadContent;

        if ($this->addUpload) {
            $arrDzFiles = [];
            if (isset($objEntry->upload)) {
                $arrDzFiles = $EntryHelper->getFileInfosFromUploadField($objEntry->upload);
            }

            $this->Template->arrDzFiles = $arrDzFiles;
            $this->Template->uploadFileExtensions = $objSettings->uploadFileExtensions;
            $this->Template->uploadFileSize = $objSettings->uploadFileSize;
            $this->Template->uploadMaxFiles = $objSettings->uploadMaxFiles;
            $this->Template->beforeUploadText = $objSettings->beforeUploadText;

            // add Dropzone-Files
            $GLOBALS['TL_CSS']['dropzone_css'] = $GLOBALS['PINBOARD_PUBLIC_FOLDER'].'/css/dropzone.css';
            $GLOBALS['TL_JAVASCRIPT']['dropzone_js'] = $GLOBALS['PINBOARD_PUBLIC_FOLDER'].'/js/dropzone.js';
        }

        // Aktions-Benachrichtigungen fürs Template setzen
        if (isset($_COOKIE[self::$COOKIE_NAME_MESSAGE])) {
            $this->Template->message = $_COOKIE[self::$COOKIE_NAME_MESSAGE];
            setcookie(self::$COOKIE_NAME_MESSAGE, '', time() - 1000000);
            unset($_COOKIE[self::$COOKIE_NAME_MESSAGE]);
        }

        if (isset($_COOKIE[self::$COOKIE_ALERT_TYPE])) {
            $this->Template->alertType = $_COOKIE[self::$COOKIE_ALERT_TYPE];
            setcookie(self::$COOKIE_ALERT_TYPE, '', time() - 1000000);
            unset($_COOKIE[self::$COOKIE_ALERT_TYPE]);
        }
    }
}
