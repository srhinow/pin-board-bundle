<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension pin-board-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\PinBoardBundle\Module\Frontend;

use Contao\BackendTemplate;
use Contao\Input;
use Contao\PageModel;
use Contao\System;
use Srhinow\PinBoardBundle\Model\PinBoardEntryModel;

class ModulePinBoardEntryDetails extends ModulePinBoardFrontend
{
    /**
     * Template.
     *
     * @var string
     */
    protected $strTemplate = 'pbb_entry_details';

    /**
     * Target pages.
     */
    protected array $arrTargets = [];

    /**
     * Display a wildcard in the back end.
     *
     * @throws \Exception
     */
    public function generate(): string
    {
        if (TL_MODE === 'BE') {
            $objTemplate = new BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### PIN BOARD EINTRAG DETAILS ###';

            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id='.$this->id;

            return $objTemplate->parse();
        }

        // Fallback template
        if ($this->pbb_fe_template) {
            $this->strTemplate = $this->pbb_fe_template;
        }

        // Set the item from the auto_item parameter
        if ($GLOBALS['TL_CONFIG']['useAutoItem'] && \Input::get('auto_item')) {
            \Input::setGet('entry', \Input::get('auto_item'));
        }

        return parent::generate();
    }

    /**
     * Generate module.
     */
    protected function compile(): void
    {
        // nur wenn die Eintrag-ID zum aktuellen member gehört
        if (null === Input::get('entry')
            || null === ($objEntry = PinBoardEntryModel::findBy(
                ['published=?', 'id=?'],
                [1, (int) Input::get('entry')]
            ))
        ) {
            return;
        }

        $EntryHelper = System::getContainer()->get('srhinow.pin_board_bundle.helper.pin_board_entry_helper');
        $this->loadLanguageFile('tl_pin_board_entry');

        // CasePage-Url generieren
        $objJumpToPage = PageModel::findByPk($this->jumpToPinBoardEntryList);
        $jumpToUrl = (null === $objJumpToPage) ? $_SERVER['REQUEST_URI'] : $objJumpToPage->getFrontendUrl();

        $this->Template->entryId = $objEntry->id;
        $this->Template->pinBoard = $this->pbb_pinBoard;
        $this->Template->toListUrl = $jumpToUrl;
        $this->Template->subject = $objEntry->subject;
        $this->Template->text = html_entity_decode($objEntry->text);
        $this->Template->hasImages = false;
        if (false !== ($arrDzFiles = $EntryHelper->getFileInfosFromUploadField($objEntry->upload))) {
            $this->Template->hasImages = true;
            $this->Template->arrDzFiles = $arrDzFiles;
        }
    }
}
