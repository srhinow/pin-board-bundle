<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension pin-board-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\PinBoardBundle\Module\Backend;

use Contao\BackendModule;

class ModulePinBoardSettings extends BackendModule
{
    /**
     * Template.
     *
     * @var string
     */
    protected $strTemplate = 'mod_properties';

    /**
     * Change the palette of the current table and switch to edit mode.
     */
    public function generate()
    {
        return $this->objDc->edit($GLOBALS['PINBOARD_SETTINGS']['ID']);
    }

    /**
     * Generate module.
     */
    protected function compile()
    {
        return '';
    }
}
