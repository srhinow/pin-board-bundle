<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension pin-board-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\PinBoardBundle\EventListener\Dca;

use Contao\Controller;
use Contao\DataContainer;

class Module
{
    /**
     * Return all info templates as array.
     */
    public function getPinBoardTemplates(DataContainer $dc): array
    {
        return Controller::getTemplateGroup('pbb_');
    }
}
