<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension pin-board-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\PinBoardBundle\EventListener\Dca;

use Contao\Controller;
use Contao\CoreBundle\Framework\ContaoFramework;
use Contao\Database;
use Contao\FilesModel;
use Contao\Input;
use Srhinow\PinBoardBundle\Model\PinBoardSettingModel;

class PinBoardSetting
{
    public $objSettings;

    public $dbSettingsId = 1;

    public function __construct(ContaoFramework $framework)
    {
        $this->framework = $framework;
        $this->framework->initialize();
        if (Database::getInstance()->tableExists('tl_pin_board_setting')) {
            $this->setObjSettings($this->getPinBoardSettings());
        }
    }

    /**
     * all dca config > onload_callback entries.
     */
    public function onLoadCallback(): void
    {
        $this->createPropertyEntry();
    }

    /**
     * create an entry if id=1 not exists.
     */
    public function createPropertyEntry(): void
    {
        $count = PinBoardSettingModel::countAll();
        if (0 === $count) {
            $objSettings = new PinBoardSettingModel();
            $objSettings->id = $this->dbSettingsId;
            $objSettings->save();
        }
    }

    /**
     * gibt alle Settings als Objekt zurück.
     */
    public function getPinBoardSettings()
    {
        $objSettings = PinBoardSettingModel::findByPk($this->dbSettingsId);
        if (null === $objSettings) {
            $this->createPropertyEntry();
            Controller::reload();
        }

        return $objSettings;
    }

    /**
     * gibt den Upload-Pfad aus den Einstellungen zurück.
     */
    public function getPinBoardUploadPath(): string
    {
        if (!Database::getInstance()->tableExists('tl_pin_board_setting')) {
            return 'var/cache';
        }

        // den Akten-Root-Pfad aus den Settings holen
        $objFolder = FilesModel::findByUuid($this->getObjSettings()->uploadFolder);
        $FolderPath = $objFolder->path;

        if (null !== ($recordId = Input::get('id'))) {
            $FolderPath .= '/'.$recordId;
        }

        return (null === $objFolder || !$FolderPath) ? 'var/cache' : $FolderPath;
    }

    public function getObjSettings(): ?PinBoardSettingModel
    {
        return $this->objSettings;
    }

    public function setObjSettings($objSettings): void
    {
        $this->objSettings = $objSettings;
    }

    public function getDbSettingsId(): int
    {
        return $this->dbSettingsId;
    }

    public function setDbSettingsId(int $dbSettingsId): void
    {
        $this->dbSettingsId = $dbSettingsId;
    }
}
