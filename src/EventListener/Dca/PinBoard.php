<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension pin-board-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\PinBoardBundle\EventListener\Dca;

use Contao\BackendUser;
use Contao\Controller;
use Contao\CoreBundle\Exception\AccessDeniedException;
use Contao\Database;
use Contao\DataContainer;
use Contao\FilesModel;
use Contao\Image;
use Contao\Input;
use Contao\Message;
use Contao\StringUtil;
use Contao\System;
use Contao\UserModel;
use Srhinow\PinBoardBundle\Helper\PinBoardHelper;
use Srhinow\PinBoardBundle\Model\PinBoardModel;

class PinBoard
{
    protected $pinBoardHelper;

    protected $pinBoardSettings;

    public function __construct(PinBoardHelper $pinBoardHelper, PinBoardSetting $pinBoardSetting)
    {
        $this->pinBoardHelper = $pinBoardHelper;
        $this->pinBoardSettings = $pinBoardSetting;
    }

    /**
     * all dca config > onload_callback entries.
     */
    public function onLoadCallback(): void
    {
        $this->checkPermission();
        $this->viewCookieMsg();
    }

    /**
     * all dca config > onsubmit_callback entries.
     */
    public function onSubmitCallback(DataContainer $dc): void
    {
        $this->setPinBoardFolder($dc);
    }

    /**
     * all dca config > onsubmit_callback entries.
     */
    public function onDeleteCallback(DataContainer $dc): void
    {
        $this->deletePinBoardEntries($dc);
        $this->dropPinBoardFolder($dc);
    }

    public function labelLabelCallback($arrRow, $strLabel, $dc, $args)
    {
        // erstellt am
        $args[2] = \strlen((string) $arrRow['dateAdded']) ? date('d.m.y', (int) $arrRow['dateAdded']) : '--';

        // Akte-Ersteller
        $objCreator = UserModel::findByPk($arrRow['createdFrom']);
        if (null !== $objCreator) {
            $args[2] .= ' <span class="hint-text" title="'.$objCreator->email.'">(von '.$objCreator->name.')</span>';
        }

        // bearbeitet am
        $args[3] = date('d.m.y H:i', (int) $arrRow['tstamp']);

        return $args;
    }

    /**
     * Auto-generate an alias if it has not been set yet.
     *
     * @param DataContainer
     *
     * @throws \Exception
     */
    public function generateAlias($varValue, DataContainer $dc): string
    {
        $autoAlias = false;

        // Generate an alias if there is none
        if ('' === $varValue) {
            $autoAlias = true;
            $varValue = standardize(StringUtil::restoreBasicEntities($dc->activeRecord->title));
        }

        $countAlias = PinBoardModel::countBy(['alias=?'], $varValue);

        // Check whether the page alias exists
        if ($countAlias > 1) {
            if (!$autoAlias) {
                throw new \Exception(sprintf($GLOBALS['TL_LANG']['ERR']['aliasExists'], $varValue));
            }

            $varValue .= '-'.$dc->id;
        }

        return $varValue;
    }

    /**
     * Check permissions to edit table tl_pin_board.
     *
     * @throws \Contao\CoreBundle\Exception\AccessDeniedException
     */
    public function checkPermission(): void
    {
        $this->User = BackendUser::getInstance();

        if ($this->User->isAdmin) {
            return;
        }

        // Set root IDs
        if (empty($this->User->pbb_pin_boards) || !\is_array($this->User->pbb_pin_boards)) {
            $root = [0];
        } else {
            $root = $this->User->pbb_pin_boards;
        }

        // den Button "neuer Fall" ausblenden, wenn die notwendigen Berechtigungen fehlen
        if (!$this->User->hasAccess('create', 'pbb_pin_boardp')) {
            $GLOBALS['TL_DCA']['tl_pin_board']['config']['closed'] = true;
        }

        /** @var \Symfony\Component\HttpFoundation\Session\SessionInterface $objSession */
        $objSession = System::getContainer()->get('session');
        $errorMsg = 'Not enough permissions to %s Laywer-Client-Portal case ID %s.';
        // Check current action
        switch (Input::get('act')) {
            case 'select':
            case 'show':
            case '':
                // Allow
                break;
            case 'edit':
                if (!$this->User->hasAccess('edit', 'pbb_pin_boardp')) {
                    throw new AccessDeniedException(sprintf($errorMsg, Input::get('act'), Input::get('id')));
                }
                break;
            case 'create':
                if (!$this->User->hasAccess('create', 'pbb_pin_boardp')) {
                    throw new AccessDeniedException(sprintf($errorMsg, Input::get('act'), Input::get('id')));
                }
                break;
            case 'copy':
                if (!$this->User->hasAccess('copy', 'pbb_pin_boardp')) {
                    throw new AccessDeniedException(sprintf($errorMsg, Input::get('act'), Input::get('id')));
                }
                // no break
            case 'delete':
                if (!$this->User->hasAccess('delete', 'pbb_pin_boardp')) {
                    throw new AccessDeniedException(sprintf($errorMsg, Input::get('act'), Input::get('id')));
                }
                break;
            case 'editAll':
            case 'deleteAll':
            case 'overrideAll':
                $session = $objSession->all();
                if ('deleteAll' === Input::get('act') && !$this->User->hasAccess('delete', 'pbb_pin_boardp')) {
                    $session['CURRENT']['IDS'] = [];
                } else {
                    $session['CURRENT']['IDS'] = array_intersect((array) $session['CURRENT']['IDS'], $root);
                }
                $objSession->replace($session);
                break;
            default:
                $errorMsg = 'Not enough permissions to %s tl_pin_board_entry.';
                if ('' !== Input::get('act')) {
                    throw new AccessDeniedException(sprintf($errorMsg, Input::get('act')));
                }
                break;
        }
    }

    public function viewCookieMsg(): void
    {
        if (isset($_COOKIE['PIN_BOARD_MESSAGE'])) {
            Message::addInfo($_COOKIE['PIN_BOARD_MESSAGE']);
            System::setCookie('PIN_BOARD_MESSAGE', '', time() - 60);
        }
    }

    /**
     * Return the edit case button.
     *
     * @param array  $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     */
    public function onEditButtonCallback($row, $href, $label, $title, $icon, $attributes): string
    {
        $this->User = BackendUser::getInstance();

        return $this->User->hasAccess('edit', 'pbb_pin_boardp') ?
            sprintf(
                '<a href="%s" title="%s" %s>%s</a>',
                Controller::addToUrl($href.'&amp;id='.$row['id'].'&amp;rt='.REQUEST_TOKEN),
                StringUtil::specialchars($title),
                $attributes,
                Image::getHtml($icon, $label)
            ) : '';
    }

    /**
     * Return the delete case button.
     *
     * @param array  $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     */
    public function onDeleteButtonCallback($row, $href, $label, $title, $icon, $attributes): string
    {
        $this->User = BackendUser::getInstance();

        return $this->User->hasAccess('delete', 'pbb_pin_boardp') ?
            sprintf(
                '<a href="%s" title="%s" %s>%s</a>',
                Controller::addToUrl($href.'&amp;id='.$row['id'].'&amp;rt='.REQUEST_TOKEN),
                StringUtil::specialchars($title),
                $attributes,
                Image::getHtml($icon, $label)
            ) : '';
    }

    /**
     * Den PinBoard-Ordner mit Settings-Pfad erzeugen und den Pfad dann in den PinBoard-Einstellungen speichern.
     *
     * @return bool|void
     */
    public function setPinBoardFolder(\DataContainer $dc)
    {
        // wenn der Pfad bereits angelegt wurde hier abbrechen
        if ('' !== $dc->activeRecord->pinBoardFolder) {
            return;
        }

        // settings holen
        $objSettings = $this->pinBoardSettings->getObjSettings();

        // den Akten-Root-Pfad aus den Settings holen
        $objFolder = FilesModel::findByUuid($objSettings->placeForPinBoards);
        if (null === $objFolder || !$objFolder->path) {
            return;
        }

        // PinBoard-Pfad zusammensetzen
        $subFolderName = $dc->activeRecord->alias ?? $dc->id;
        $pinBoardPath = $objFolder->path.'/'.StringUtil::standardize($subFolderName);

        // falls nicht vorhanden den Speicherort-Ordner anlegen
        if (!file_exists(TL_ROOT.'/'.$pinBoardPath)) {
            if (!mkdir(TL_ROOT.'/'.$pinBoardPath)) {
                return 'Folder '.TL_ROOT.'/'.$pinBoardPath.' existiert nicht';
            }

            \Dbafs::addResource($pinBoardPath);
        }

        // aktuelle PinBoard-Instanz holen
        $objPinBoard = PinBoardModel::findByPk($dc->id);
        if (null === $objPinBoard) {
            return;
        }

        // Pfad zum PinBoard speichern
        $objPinBoard->pinBoardFolder = $pinBoardPath;
        $objPinBoard->save();
    }

    /**
     * PinBoard-Einträge löschen.
     */
    public function deletePinBoardEntries(DataContainer $dc): void
    {
        if (null === PinBoardModel::findByPk($dc->id)) {
            return;
        }

        Database::getInstance()
            ->prepare('DELETE FROM `tl_pin_board_entry` WHERE `pid`=?')
            ->execute($dc->id)
        ;
    }

    /**
     * beim Löschen eines Pin-Boards auch den Ordner mit allen Dateien der Einträge löschen.
     */
    public function dropPinBoardFolder(DataContainer $dc): void
    {
        $this->pinBoardHelper->dropPinBoardFolder($dc->id);
    }
}
