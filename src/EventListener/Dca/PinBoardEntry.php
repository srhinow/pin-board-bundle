<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension pin-board-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\PinBoardBundle\EventListener\Dca;

use Contao\Backend;
use Contao\Controller;
use Contao\CoreBundle\Exception\AccessDeniedException;
use Contao\DataContainer;
use Contao\Input;
use Contao\StringUtil;
use Contao\System;
use Srhinow\PinBoardBundle\Model\PinBoardEntryModel;
use Srhinow\PinBoardBundle\Model\PinBoardModel;

class PinBoardEntry extends Backend
{
    /**
     * Import the back end user object.
     */
    public function __construct()
    {
        parent::__construct();
        $this->import('BackendUser', 'User');
    }

    /**
     * all dca config > onload_callback entries.
     */
    public function onLoadCallback(DataContainer $dc): void
    {
        $this->checkPermission();
        $this->openFile();
        $this->setRightPathToUpload($dc);
        // $this->setChangesForAllRecordLists($dc);
    }

    /**
     * all dca config > onsubmit_callback entries.
     *
     * @throws \Exception
     */
    public function onSubmitCallback(DataContainer $dc): void
    {
//        $this->cleanRecordFiles($dc);
//        $this->sendNewRecordNotification($dc);
    }

    public function labelLabelCallback($arrRow, $strLabel, $dc, $args)
    {
        if ($arrRow['new']) {
            foreach ($args as $k => $arg) {
                $args[$k] = '<span class="strong">'.$arg.'</span>';
            }
        }

        return $args;
    }

    /**
     * if click on file-link from a record.
     */
    public function openFile(): void
    {
        $path = Input::get('file');
        if ('openfile' !== Input::get('key') || !$path) {
            return;
        }

        Controller::sendFileToBrowser($path);
    }

    public function setRightPathToUpload(DataContainer $dc): void
    {
        if (null === ($objPinBoardEntry = PinBoardEntryModel::findByPk($dc->id))) {
            return;
        }

        // für den File-Path die Eltern-Tabelle holen
        $objPinBoard = PinBoardModel::findByPk($objPinBoardEntry->pid);
        if (null === $objPinBoard || '' === $objPinBoard->pinBoardFolder) {
            return;
        }

        $GLOBALS['TL_DCA']['tl_pin_board_entry']['fields']['upload']['eval']['uploadFolder'] = $objPinBoard->pinBoardFolder;
    }

    /**
     * format child-entries in list-view.
     *
     * @param array $arrRow
     */
    public function onSortingChildRecordCallback($arrRow): string
    {
        $strContent = $arrRow['email'].':&nbsp;'.StringUtil::substr($arrRow['subject'], 100);
        $ifUploads = false;

        // falls es auch uploads gibt
        if ($arrLinks = $this->getLinksFromUpload($arrRow['upload'])) {
            $ifUploads = true;
        }

        if (true === $ifUploads) {
            $strContent .= '<br><strong>Dateien:</strong> '.implode(', ', $arrLinks);
        }

        // CSS-Klassen setzen
        $strClasses = 'case_record';

        if ('1' === $arrRow['published']) {
            $strClasses .= ' published';
        }

        return sprintf('<div class="'.$strClasses.'">%s</div>', $strContent);
    }

    /**
     * @param string $uploadField
     *
     * @return array|bool
     */
    public function getLinksFromUpload($uploadField)
    {
        $arrLinks = [];

        if (null === $uploadField) {
            return $arrLinks;
        }

        $arrUploads = unserialize($uploadField);
        if (!\is_array($arrUploads)) {
            $arrUploads = [$uploadField];
        }

        foreach ($arrUploads as $k => $upload) {
            if (!\strlen($upload)) {
                continue;
            }
            $objFile = \FilesModel::findByUuid($upload);

            $url = Controller::addToUrl('key=openfile&file='.$objFile->path);
            $arrLinks[] = '<a href="'.$url.'">'.$objFile->name.'</a>';
        }

        return (\count($arrLinks) < 0) ? false : $arrLinks;
    }

    /**
     * Check permissions to edit table tl_pin_board_entry.
     */
    public function checkPermission(): void
    {
        $bundles = System::getContainer()->getParameter('kernel.bundles');

        if ($this->User->isAdmin) {
            return;
        }

        // Set root IDs
        if (empty($this->User->pbb_pin_board_entrys) || !\is_array($this->User->pbb_pin_board_entrys)) {
            $root = [0];
        } else {
            $root = $this->User->pbb_pin_board_entrys;
        }

        // den Button "neuer Fall" ausblenden wenn die notwendigen Berechtigungen fehlen
        if (!$this->User->hasAccess('create', 'pbb_pin_board_entryp')) {
            $GLOBALS['TL_DCA']['tl_pin_board_entry']['config']['closed'] = true;
        }

        /** @var Symfony\Component\HttpFoundation\Session\SessionInterface $objSession */
        $objSession = System::getContainer()->get('session');

        // Check current action
        switch (Input::get('act')) {
            case 'select':
            case 'show':
            case '':
                // Allow
                break;
            case 'edit':
                if (!$this->User->hasAccess('edit', 'pbb_pin_board_entryp')) {
                    throw new AccessDeniedException('Not enough permissions to '.Input::get('act').' pin-board entry ID '.Input::get('id').'.');
                }
                break;
            case 'create':
                if (!$this->User->hasAccess('create', 'pbb_pin_board_entryp')) {
                    throw new AccessDeniedException('Not enough permissions to '.Input::get('act').' pin-board entry ID '.Input::get('id').'.');
                }
                break;
            case 'copy':
                if (!$this->User->hasAccess('copy', 'pbb_pin_board_entryp')) {
                    throw new AccessDeniedException('Not enough permissions to '.Input::get('act').' pin-board entry ID '.Input::get('id').'.');
                }
                // no break
            case 'delete':
                if (!$this->User->hasAccess('delete', 'pbb_pin_board_entryp')) {
                    throw new AccessDeniedException('Not enough permissions to '.Input::get('act').' pin-board entry ID '.Input::get('id').'.');
                }
                break;
            case 'editAll':
            case 'deleteAll':
            case 'overrideAll':
                $session = $objSession->all();
                if ('deleteAll' === Input::get('act') && !$this->User->hasAccess('delete', 'pbb_pin_board_entryp')) {
                    $session['CURRENT']['IDS'] = [];
                } else {
                    $session['CURRENT']['IDS'] = array_intersect((array) $session['CURRENT']['IDS'], $root);
                }
                $objSession->replace($session);
                break;
            default:
                if ('' !== Input::get('act')) {
                    throw new AccessDeniedException('Not enough permissions to '.Input::get('act').' tl_pin_board_entry.');
                }
                break;
        }
    }
}
