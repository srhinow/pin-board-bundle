<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension pin-board-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\PinBoardBundle\EventListener\Cron;

use Contao\CoreBundle\Framework\ContaoFramework;
use Contao\Database;
use Srhinow\PinBoardBundle\EventListener\Dca\PinBoardSetting;
use Srhinow\PinBoardBundle\Helper\PinBoardEntryHelper;
use Srhinow\PinBoardBundle\Model\PinBoardEntryModel;

class PinBoardCron
{
    protected $pinBoardSettings;

    protected $entryHelper;

    private $framework;

    public function __construct(PinBoardSetting $pinBoardSettings, PinBoardEntryHelper $entryHelper, ContaoFramework $framework)
    {
        $this->pinBoardSettings = $pinBoardSettings;
        $this->entryHelper = $entryHelper;
        $this->framework = $framework;
        $this->framework->initialize();
    }

    public function fixedEmptyExpiredDates(): void
    {
        $objSettings = $this->pinBoardSettings->getObjSettings();

        // Wenn es auf 0 steht, wird nie etwas automatisch durch den CRON gelöscht
        if (0 === (int) $objSettings->closedAfterDays) {
            return;
        }

        Database::getInstance()->prepare('UPDATE tl_pin_board_entry %s WHERE dateExpired = ?')
            ->set(['dateExpired' => strtotime('+'.$objSettings->closedAfterDays.' days')])
            ->execute(0);
    }

    /**
     * lösche alle Pinnwand-Einträge, wenn die Aktiv-Tage-Anzahl überschritten ist.
     */
    public function deleteExpiredEntries(): void
    {
        $objSettings = $this->pinBoardSettings->getObjSettings();

        // Wenn es auf 0 steht, wird nie etwas automatisch durch den CRON gelöscht
        if (0 === (int) $objSettings->closedAfterDays) {
            return;
        }

        if (null === ($objEntries = PinBoardEntryModel::findBy(
            ['dateExpired < ?'],
            [time()]
        ))) {
            return;
        }

        while ($objEntries->next()) {
            $objEntry = $objEntries->current();

            $this->entryHelper->dropPinBoardEntryFolder($objEntry->id);

            $objEntry->delete();
        }
    }
}
