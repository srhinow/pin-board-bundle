<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension pin-board-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Table tl_pin_board_entry
 */

$GLOBALS['TL_DCA']['tl_pin_board_entry'] = [
    // Config
    'config' => [
        'dataContainer' => 'Table',
        'ptable' => 'tl_pin_board',
        'enableVersioning' => true,
        'sql' => [
            'keys' => [
                'id' => 'primary',
                'pid' => 'index',
            ],
        ],
    ],
    // List
    'list' => [
        'sorting' => [
            'mode' => 4,
            'headerFields' => ['subject', 'memberId:tl_member.email', 'published'],
            'fields' => ['tstamp DESC'],
            'panelLayout' => 'filter;sort,search,limit',
        ],
        'global_operations' => [
            'all' => [
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset();"',
            ],
        ],
        'operations' => [
            'edit' => [
                'href' => 'act=edit',
                'icon' => 'edit.svg',
            ],
            'show' => [
                'href' => 'key=readMessage',
                'icon' => $GLOBALS['PINBOARD_PUBLIC_FOLDER'].'/icons/table_tab_search.png',
            ],
            'delete' => [
                'href' => 'act=delete',
                'icon' => 'delete.svg',
                'attributes' => 'onclick="if(!confirm(\''.($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null).'\'))return false;Backend.getScrollOffset()"',
            ],
        ],
    ],
    // Palettes
    //        ;{upload_legend:hide},upload

    'palettes' => [
        'default' => '
        {main_legend},pid
        ;{message_legend},subject,text
        ;{upload_legend},upload
        ;{protected_legend:hide},memberId
        ;{extend_legend},published,new,dateExpired
        ',
    ],
    // Fields
    'fields' => [
        'id' => [
            'sql' => 'int(10) unsigned NOT NULL auto_increment',
        ],
        'pid' => [
            'search' => true,
            'inputType' => 'select',
            'foreignKey' => 'tl_pin_board.title',
            'eval' => [
                'mandatory' => true,
                'chosen' => true,
                'submitOnChange' => true,
                'includeBlankOption' => true,
                'tl_class' => 'w50',
            ],
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'relation' => ['type' => 'belongsTo', 'load' => 'lazy'],
        ],
        'tstamp' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'flag' => 8,
            'eval' => ['mandatory' => true, 'rgxp' => 'datim'],
        ],
        'modify' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'flag' => 8,
            'eval' => [
                'mandatory' => true,
                'tl_class' => 'wizard w50', 'minlength' => 1,
                'maxlength' => 10, 'rgxp' => 'date',
            ],
        ],
        'dateAdded' => [
            'default' => time(),
            'flag' => 6,
            'sorting' => true,
            'eval' => ['rgxp' => 'date', 'doNotCopy' => true],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'dateExpired' => [
            'flag' => 6,
            'sorting' => true,
            'rgxp' => 'datim',
            'inputType' => 'text',
            'eval' => [
                'rgxp' => 'datim',
                'datepicker' => $this->getDatePickerString(),
                'tl_class' => 'clr wizard w50',
            ],
            'tl_class' => 'clr wizard w50',
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'memberId' => [
            'foreignKey' => 'tl_member.CONCAT(lastname, \' \', firstname, \' \', email,\' \',id)',
            'inputType' => 'select',
            'relation' => ['type' => 'hasOne', 'load' => 'lazy'],
            'eval' => [
                'doNotCopy' => true,
                'mandatory' => false,
                'chosen' => true,
                'includeBlankOption' => true,
                'submitOnChange' => true,
                'tl_class' => 'w50',
            ],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'authorName' => [
            'search' => true,
            'inputType' => 'text',
            'eval' => ['maxlength' => 255, 'tl_class' => 'w50', 'readonly' => true],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'email' => [
            'search' => true,
            'inputType' => 'text',
            'eval' => ['maxlength' => 255, 'tl_class' => 'w50', 'readonly' => true],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'subject' => [
            'search' => true,
            'inputType' => 'text',
            'eval' => ['maxlength' => 255, 'tl_class' => 'full'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'activateToken' => [
            'search' => true,
            'inputType' => 'text',
            'eval' => ['maxlength' => 255, 'tl_class' => 'full'],
            'sql' => 'varchar(255) NULL',
        ],
        'editToken' => [
            'search' => true,
            'inputType' => 'text',
            'eval' => ['maxlength' => 255, 'tl_class' => 'full'],
            'sql' => 'varchar(255) NULL',
        ],
        'deleteToken' => [
            'search' => true,
            'inputType' => 'text',
            'eval' => ['maxlength' => 255, 'tl_class' => 'full'],
            'sql' => 'varchar(255) NULL',
        ],
        'text' => [
            'search' => true,
            'default' => '',
            'inputType' => 'textarea',
            'eval' => [
                'tl_class' => 'clr full',
                'rte' => 'tinyMCE',
                'style' => 'height:100px',
            ],
            'sql' => 'text NULL',
        ],
        'upload' => [
            'inputType' => 'fineUploader',
            'eval' => [
                // Mandatory to store the file on the server
                'storeFile' => true,
                // Allow multiple files to be uploaded
                'multiple' => true,
                // Upload target directory (can also be a Contao file system UUID)
                'uploadFolder' => \Contao\System::getContainer()
                    ->get('srhinow.pin_board_bundle.listeners.dca.pin_board_setting')
                    ->getPinBoardUploadPath(),
                // Upload to the FE member home directory (overrides "uploadFolder",
                // can also be a Contao file system UUID)
                'useHomeDir' => false,
                // Maximum files that can be uploaded
                'uploaderLimit' => 10,
                // Add files to the database assisted file system
                'addToDbafs' => true,
                'extensions' => &$GLOBALS['TL_CONFIG']['uploadTypes'],           // Allowed extension types
                // 'minlength'         => 1048000, // Minimum file size
                // 'maxlength'         => 2048000, // Maximum file size (is ignored if you use chunking!)
                // 'maxWidth'          => 800, // Maximum width (applies to images only)
                // 'maxHeight'         => 600, // Maximum height (applies to images only)
                // Do not overwrite files in destination folder
                'doNotOverwrite' => true,
                // Custom upload button label
                'uploadButtonLabel' => &$GLOBALS['TL_LANG']['tl_pin_board_entry']['upload_button_text'],
                // Enable chunking
                'chunking' => true,
                // Chunk size in bytes
                'chunkSize' => 2000000,
                // Allow multiple chunks to be uploaded simultaneously per file
                'concurrent' => true,
                // Maximum allowable concurrent requests
                'maxConnections' => 5,
                // Upload the files directly to the destination folder. If not set, then the files are first uploaded
                // to the temporary folder and moved to the destination folder only when the form is submitted
                // 'directUpload' => true,
                // Set a custom thumbnail image size that is generated upon image upload
                // 'imageSize'         => [160, 120, 'center_center'],

                // You can also use the default features of fileTree widget such as:
                // isGallery, isDownloads

                // The "orderField" attribute is not valid (see #9)
            ],
            'sql' => 'blob NULL',
        ],
        'new' => [
            'inputType' => 'checkbox',
            'filter' => true,
            'eval' => ['doNotCopy' => true, 'tl_class' => 'w50'],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'published' => [
            'exclude' => true,
            'filter' => true,
            'sorting' => true,
            'default' => 1,
            'inputType' => 'checkbox',
            'eval' => ['doNotCopy' => true, 'tl_class' => 'clr w50'],
            'sql' => "char(1) NOT NULL default ''",
        ],
    ],
];
