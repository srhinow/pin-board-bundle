<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension pin-board-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Palettes
 */
$GLOBALS['TL_DCA']['tl_module']['palettes']['pbb_fe_pin_board_entry_form'] = '
    {title_legend},name,type,pbb_pinBoard;
    {notification_legend},notificationId,activateEntryPage,editEntryPage,deleteEntryPage;
    {jumpTo_Legend},jumpToPinBoardEntryList;
    {upload_Legend},addUpload,beforeUploadContent;
    {template_legend},pbb_fe_template;
    {protected_legend:hide},protected;
    {expert_legend:hide},cssID,space';

$GLOBALS['TL_DCA']['tl_module']['palettes']['pbb_fe_pin_board_entry_details'] = '
    {title_legend},name,type,noEntryContent;
    {jumpTo_Legend},jumpToPinBoardEntryList;
    {template_legend},pbb_fe_template;
    {protected_legend:hide},protected;
    {expert_legend:hide},cssID,space';

$GLOBALS['TL_DCA']['tl_module']['palettes']['pbb_fe_entry_list'] = '
    {title_legend},name,type, pbb_pinBoard, pbb_numberOfItems, perPage, onlyMemberEntries,noEntryContent;
    {template_legend},pbb_fe_template;
    {jumpTo_Legend},jumpToEntryDetails,jumpToNewEntryForm,jumpToEditEntryForm;
    {protected_legend:hide},protected;
    {expert_legend:hide},cssID,space';

$GLOBALS['TL_DCA']['tl_module']['palettes']['pbb_activate_entry'] = '
    {title_legend},name,type, noEntryForToken,successfullyActionForToken;
    {template_legend},pbb_fe_template;
    {expert_legend:hide},cssID,space';

$GLOBALS['TL_DCA']['tl_module']['palettes']['pbb_delete_entry'] = '
    {title_legend},name,type, noEntryForToken,successfullyActionForToken;
    {template_legend},pbb_fe_template;
    {expert_legend:hide},cssID,space';

/*
 * Fields
 */
$GLOBALS['TL_DCA']['tl_module']['fields']['pbb_fe_template'] = [
    'exclude' => true,
    'inputType' => 'select',
    'eval' => ['tl_class' => 'w50'],
    'sql' => "varchar(32) NOT NULL default ''",
];

$GLOBALS['TL_DCA']['tl_module']['fields']['pbb_pinBoard'] = [
    'search' => true,
    'inputType' => 'select',
    'foreignKey' => 'tl_pin_board.title',
    'eval' => [
        'mandatory' => true,
        'chosen' => true,
        'submitOnChange' => true,
        'includeBlankOption' => true,
        'tl_class' => 'w50',
    ],
    'sql' => 'int(10) unsigned NULL',
    'relation' => ['type' => 'belongsTo', 'load' => 'lazy'],
];

$GLOBALS['TL_DCA']['tl_module']['fields']['pbb_numberOfItems'] = [
    'default' => 3,
    'exclude' => true,
    'inputType' => 'text',
    'eval' => ['mandatory' => true, 'rgxp' => 'digit', 'tl_class' => 'w50'],
    'sql' => "smallint(5) unsigned NOT NULL default '0'",
];
$GLOBALS['TL_DCA']['tl_module']['fields']['jumpToPinBoardList'] = [
    'exclude' => true,
    'inputType' => 'pageTree',
    'foreignKey' => 'tl_page.title',
    'eval' => ['fieldType' => 'radio'],
    'sql' => 'int(10) unsigned NOT NULL default 0',
    'relation' => ['type' => 'hasOne', 'load' => 'lazy'],
];
$GLOBALS['TL_DCA']['tl_module']['fields']['jumpToPinBoardEntryList'] = [
    'exclude' => true,
    'inputType' => 'pageTree',
    'foreignKey' => 'tl_page.title',
    'eval' => ['fieldType' => 'radio'],
    'sql' => 'int(10) unsigned NOT NULL default 0',
    'relation' => ['type' => 'hasOne', 'load' => 'lazy'],
];
$GLOBALS['TL_DCA']['tl_module']['fields']['jumpToNewEntryForm'] = [
    'exclude' => true,
    'inputType' => 'pageTree',
    'foreignKey' => 'tl_page.title',
    'eval' => ['fieldType' => 'radio'],
    'sql' => 'int(10) unsigned NOT NULL default 0',
    'relation' => ['type' => 'hasOne', 'load' => 'lazy'],
];
$GLOBALS['TL_DCA']['tl_module']['fields']['jumpToEditEntryForm'] = [
    'exclude' => true,
    'inputType' => 'pageTree',
    'foreignKey' => 'tl_page.title',
    'eval' => ['fieldType' => 'radio'],
    'sql' => 'int(10) unsigned NOT NULL default 0',
    'relation' => ['type' => 'hasOne', 'load' => 'lazy'],
];
$GLOBALS['TL_DCA']['tl_module']['fields']['jumpToEntryDetails'] = [
    'exclude' => true,
    'inputType' => 'pageTree',
    'foreignKey' => 'tl_page.title',
    'eval' => ['fieldType' => 'radio'],
    'sql' => 'int(10) unsigned NOT NULL default 0',
    'relation' => ['type' => 'hasOne', 'load' => 'lazy'],
];
$GLOBALS['TL_DCA']['tl_module']['fields']['activateEntryPage'] = [
    'exclude' => true,
    'inputType' => 'pageTree',
    'foreignKey' => 'tl_page.title',
    'eval' => ['fieldType' => 'radio', 'tl_class' => 'clr'],
    'sql' => 'int(10) unsigned NOT NULL default 0',
    'relation' => ['type' => 'hasOne', 'load' => 'lazy'],
];
$GLOBALS['TL_DCA']['tl_module']['fields']['editEntryPage'] = [
    'exclude' => true,
    'inputType' => 'pageTree',
    'foreignKey' => 'tl_page.title',
    'eval' => ['fieldType' => 'radio'],
    'sql' => 'int(10) unsigned NOT NULL default 0',
    'relation' => ['type' => 'hasOne', 'load' => 'lazy'],
];
$GLOBALS['TL_DCA']['tl_module']['fields']['deleteEntryPage'] = [
    'exclude' => true,
    'inputType' => 'pageTree',
    'foreignKey' => 'tl_page.title',
    'eval' => ['fieldType' => 'radio'],
    'sql' => 'int(10) unsigned NOT NULL default 0',
    'relation' => ['type' => 'hasOne', 'load' => 'lazy'],
];
$GLOBALS['TL_DCA']['tl_module']['fields']['addUpload'] = [
    'exclude' => true,
    'inputType' => 'checkbox',
    'sql' => "char(1) NOT NULL default ''",
];
$GLOBALS['TL_DCA']['tl_module']['fields']['onlyMemberEntries'] = [
    'exclude' => true,
    'inputType' => 'checkbox',
    'eval' => ['tl_class' => 'm12 w50'],
    'sql' => "char(1) NOT NULL default ''",
];
$GLOBALS['TL_DCA']['tl_module']['fields']['noEntryContent'] = [
    'default' => '',
    'search' => true,
    'inputType' => 'textarea',
    'eval' => ['maxlength' => 500, 'tl_class' => 'clr full', 'rte' => 'tinyMCE', 'style' => 'height:100px'],
    'sql' => 'text NULL',
];
$GLOBALS['TL_DCA']['tl_module']['fields']['beforeUploadContent'] = [
    'default' => '',
    'search' => true,
    'inputType' => 'textarea',
    'eval' => ['maxlength' => 500, 'tl_class' => 'clr full', 'rte' => 'tinyMCE', 'style' => 'height:100px'],
    'sql' => 'text NULL',
];
$GLOBALS['TL_DCA']['tl_module']['fields']['notificationId'] = [
    'exclude' => true,
    'inputType' => 'select',
    'foreignKey' => 'tl_nc_notification.title',
    'eval' => [
        'includeBlankOption' => true,
        'tl_class' => 'w50',
        'submitOnChange' => false,
    ],
    'sql' => "int(10) unsigned NOT NULL default '0'",
    'relation' => ['type' => 'hasOne', 'load' => 'lazy'],
];
$GLOBALS['TL_DCA']['tl_module']['fields']['noEntryForToken'] = [
    'default' => '',
    'search' => true,
    'inputType' => 'textarea',
    'eval' => ['maxlength' => 500, 'tl_class' => 'clr full', 'rte' => 'tinyMCE', 'style' => 'height:100px'],
    'sql' => 'text NULL',
];
$GLOBALS['TL_DCA']['tl_module']['fields']['successfullyActionForToken'] = [
    'default' => '',
    'search' => true,
    'inputType' => 'textarea',
    'eval' => ['maxlength' => 500, 'tl_class' => 'clr full', 'rte' => 'tinyMCE', 'style' => 'height:100px'],
    'sql' => 'text NULL',
];
