<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension pin-board-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Table tl_user.
 */

// Extend the default palettes
Contao\CoreBundle\DataContainer\PaletteManipulator::create()
    ->addLegend(
        'pin_board_legend',
        'tl_user',
        Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_BEFORE
    )
    ->addField(
        [
            'pbb_pin_boardp',
            'pbb_pin_board_entryp',
        ],
        'pin_board_legend',
        Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_APPEND
    )
    ->applyToPalette('extend', 'tl_user')
    ->applyToPalette('custom', 'tl_user')
;

// Add fields to tl_user
$GLOBALS['TL_DCA']['tl_user']['fields']['pbb_pin_boardp'] = [
    'exclude' => true,
    'inputType' => 'checkbox',
    'options' => ['edit', 'create', 'delete'],
    'reference' => &$GLOBALS['TL_LANG']['MSC'],
    'eval' => ['multiple' => true],
    'sql' => 'blob NULL',
];

$GLOBALS['TL_DCA']['tl_user']['fields']['pbb_pin_board_entryp'] = [
    'exclude' => true,
    'inputType' => 'checkbox',
    'options' => ['edit', 'create', 'delete'],
    'reference' => &$GLOBALS['TL_LANG']['MSC'],
    'eval' => ['multiple' => true],
    'sql' => 'blob NULL',
];
