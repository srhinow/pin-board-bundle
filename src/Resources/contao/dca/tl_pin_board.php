<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension pin-board-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Table tl_pin_board.
 */
$GLOBALS['TL_DCA']['tl_pin_board'] = [
    // Config
    'config' => [
        'dataContainer' => 'Table',
        'ctable' => ['tl_pin_board_entry'],
        'sql' => [
            'keys' => [
                'id' => 'primary',
                'alias' => 'index',
            ],
        ],
    ],
    // List
    'list' => [
        'sorting' => [
            'mode' => 2,
            'fields' => ['dateAdded DESC'],
            'flag' => 1,
            'panelLayout' => 'filter;sort,search,limit',
        ],
        'label' => [
            'fields' => ['title', 'dateAdded', 'tstamp', 'published'],
            'showColumns' => true,
        ],
        'global_operations' => [
            'all' => [
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset();"',
            ],
        ],
        'operations' => [
            'edit' => [
                'label' => &$GLOBALS['TL_LANG']['tl_pin_board']['edit'],
                'href' => 'act=edit',
                'icon' => 'edit.gif',
            ],
            'entries' => [
                'label' => &$GLOBALS['TL_LANG']['tl_pin_board']['entries'],
                'href' => 'table=tl_pin_board_entry',
                'icon' => $GLOBALS['PINBOARD_PUBLIC_FOLDER'].'/icons/combo_boxes.png',
            ],
            'delete' => [
                'label' => &$GLOBALS['TL_LANG']['tl_pin_board']['delete'],
                'href' => 'act=delete',
                'icon' => 'delete.gif',
                'attributes' => 'onclick="if(!confirm(\''.($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null).'\'))return false;Backend.getScrollOffset()"',
            ],
        ],
    ],
    // Palettes
    'palettes' => [
        'default' => '
            {meta_legend},title,alias
            ;{published_legend},published
            ;{expert_legend:hide},pinBoardFolder
            ',
    ],
    // Fields
    'fields' => [
        'id' => [
            'sql' => 'int(10) unsigned NOT NULL auto_increment',
        ],
        'title' => [
            'exclude' => true,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => ['tl_class' => 'w50'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'alias' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => ['rgxp' => 'alias', 'doNotCopy' => true, 'unique' => true, 'maxlength' => 255, 'tl_class' => 'w50'],
            'save_callback' => [
                ['srhinow.pin_board_bundle.listeners.dca.pin_board', 'generateAlias'],
            ],
            'sql' => "varchar(255) BINARY NOT NULL default ''",
        ],
        'tstamp' => [
            'label' => &$GLOBALS['TL_LANG']['tl_pin_board']['tstamp'],
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'flag' => 8,
            'eval' => ['mandatory' => true, 'rgxp' => 'date'],
        ],
        'modify' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'flag' => 8,
            'eval' => [
                'mandatory' => true,
                'datepicker' => $this->getDatePickerString(),
                'tl_class' => 'wizard w50',
                'minlength' => 1,
                'maxlength' => 10,
                'rgxp' => 'date',
            ],
        ],
        'dateAdded' => [
            'default' => time(),
            'sorting' => true,
            'flag' => 6,
            'eval' => ['rgxp' => 'date', 'doNotCopy' => true],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'createdFrom' => [
            'default' => BackendUser::getInstance()->id,
            'search' => true,
            'filter' => true,
            'sorting' => true,
            'foreignKey' => 'tl_user.name',
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'relation' => ['type' => 'hasOne', 'load' => 'lazy'],
        ],
        'published' => [
            'inputType' => 'checkbox',
            'default' => 1,
            'exclude' => true,
            'filter' => true,
            'sorting' => true,
            'eval' => ['doNotCopy' => true, 'tl_class' => 'clr w50'],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'pinBoardFolder' => [
            'inputType' => 'text',
            'exclude' => true,
            'search' => true,
            'sorting' => true,
            'eval' => ['tl_class' => 'w50', 'readonly' => true],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
    ],
];
