<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension pin-board-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Table tl_pin_board_setting.
 */
$GLOBALS['TL_DCA']['tl_pin_board_setting'] = [
    // Config
    'config' => [
        'dataContainer' => 'Table',
        'enableVersioning' => false,
        'sql' => [
            'keys' => [
                'id' => 'primary',
            ],
        ],
        'onload_callback' => [
            ['srhinow.pin_board_bundle.listeners.dca.pin_board_setting', 'onLoadCallback'],
        ],
    ],
    // List
    'list' => [
        'sorting' => [
            'mode' => 4,
            'panelLayout' => 'filter;search,limit',
        ],
        'label' => [
            'fields' => ['memberGroupId'],
            'format' => '%s (%s)',
        ],
    ],
    // Palettes
    'palettes' => [
        'default' => '
            {groups_legend},memberGroupId
            ;{folder_legend},placeForPinBoards,tmpFolder,uploadFolder
            ;{notification_legend},activateNotification
            ;{upload_legend},uploadFileExtensions,uploadFileSize,uploadMaxFiles
            ;{cron_legend},closedAfterDays
        ',
    ],

    // Fields
    'fields' => [
        'id' => [
            'sql' => 'int(10) unsigned NOT NULL auto_increment',
        ],
        'tstamp' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'modify' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'memberGroupId' => [
            'inputType' => 'select',
            'foreignKey' => 'tl_member_group.name',
            'eval' => [
                'chosen' => true,
                'includeBlankOption' => true,
                'tl_class' => 'w50',
            ],
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'relation' => ['type' => 'hasOne', 'load' => 'lazy'],
        ],
        'placeForPinBoards' => [
            'exclude' => true,
            'inputType' => 'fileTree',
            'eval' => [
                'mandatory' => true,
                'multiple' => false,
                'fieldType' => 'radio',
                'tl_class' => 'clr',
            ],
            'sql' => 'blob NULL',
        ],
        'uploadFolder' => [
            'exclude' => true,
            'inputType' => 'fileTree',
            'eval' => [
                'mandatory' => true,
                'multiple' => false,
                'fieldType' => 'radio',
                'tl_class' => 'clr',
            ],
            'sql' => 'blob NULL',
        ],
        'uploadFileExtensions' => [
            'inputType' => 'text',
            'default' => '.jpeg,.png,.pdf,.docx,.odt,.doc',
            'eval' => ['maxlength' => 255, 'tl_class' => 'w50'],
            'sql' => "varchar(255) NOT NULL default '.jpg,.jpeg,.png,.pdf,.docx,.odt,.doc'",
        ],
        'uploadFileSize' => [
            'inputType' => 'text',
            'default' => 5,
            'eval' => ['tl_class' => 'w50'],
            'sql' => "int(10) unsigned NOT NULL default '5'",
        ],
        'uploadMaxFiles' => [
            'inputType' => 'text',
            'default' => 5,
            'eval' => ['tl_class' => 'w50'],
            'sql' => "int(10) unsigned NOT NULL default '5'",
        ],
        'closedAfterDays' => [
            'search' => true,
            'inputType' => 'text',
            'default' => '30',
            'eval' => ['maxlength' => 25, 'tl_class' => 'w50'],
            'sql' => "varchar(25) NOT NULL default '30'",
        ],
    ],
];
