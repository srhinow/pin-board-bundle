<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension pin-board-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Fields.
 */
$GLOBALS['TL_LANG']['tl_pin_board']['title'] = ['Pinnwand-Name'];
$GLOBALS['TL_LANG']['tl_pin_board']['tstamp'] = ['Bearbeitet am'];
$GLOBALS['TL_LANG']['tl_pin_board']['dateAdded'] = ['Erstellt am'];
$GLOBALS['TL_LANG']['tl_pin_board']['createdFrom'] = ['erstellt von'];

$GLOBALS['TL_LANG']['tl_pin_board']['memberId'] = [
    'Mandant',
    'Wählen Sie den zur Pinnwand gehörenden Mandanten aus.',
];

$GLOBALS['TL_LANG']['tl_pin_board']['published'] = ['Pinnwand aktiv'];

$GLOBALS['TL_LANG']['tl_pin_board']['pinBoardFolder'] = [
    'Pinnwand-Speicherort',
    'Wird beim anlegen einer Pinnwand automatisch durch den Speicherort aus den
    Einstellungen und dem Alias erstellt und hier gespeichert.',
];

/*
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_pin_board']['new'][0] = 'Neue Pinnwand';
$GLOBALS['TL_LANG']['tl_pin_board']['new'][1] = 'Eine neue Pinnwand anlegen.';
$GLOBALS['TL_LANG']['tl_pin_board']['edit'][0] = 'Pinnwand bearbeiten';
$GLOBALS['TL_LANG']['tl_pin_board']['edit'][1] = 'Pinnwand ID %s bearbeiten.';
$GLOBALS['TL_LANG']['tl_pin_board']['entries'][0] = 'Anzeigen anzeigen';
$GLOBALS['TL_LANG']['tl_pin_board']['entries'][1] = 'Anzeigen dieser Pinnwand anzeigen.';
$GLOBALS['TL_LANG']['tl_pin_board']['delete'][0] = 'Pinnwand löschen';
$GLOBALS['TL_LANG']['tl_pin_board']['delete'][1] = 'Pinnwand ID %s löschen.';
$GLOBALS['TL_LANG']['tl_pin_board']['multiEdit'][0] = 'mehrere bearbeiten';

/*
 * Legends
 */
$GLOBALS['TL_LANG']['tl_pin_board']['meta_legend'] = 'Grund-Informationen';
$GLOBALS['TL_LANG']['tl_pin_board']['published_legend'] = 'Veröffentlichungs-Einstellungen';
$GLOBALS['TL_LANG']['tl_pin_board']['expert_legend'] = 'Experten-Einstellungen';

/*
 * Messages
 */
$GLOBALS['TL_LANG']['tl_pin_board']['no_entries_msg'] = 'Es sind keine Pinnwände vorhanden.';
