<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension pin-board-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

$GLOBALS['TL_LANG']['MSC']['edit'] = 'Bearbeiten';
$GLOBALS['TL_LANG']['MSC']['copy'] = 'Kopieren';
