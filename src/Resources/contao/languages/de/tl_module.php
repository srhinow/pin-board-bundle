<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension pin-board-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Legends
 */
$GLOBALS['TL_LANG']['tl_module']['jumpTo_Legend'] = 'Weiterleitungen';
$GLOBALS['TL_LANG']['tl_module']['notification_legend'] = 'Benachrichtigung';

/*
 * Fields
 */
$GLOBALS['TL_LANG']['tl_module']['pbb_fe_template'] = ['Template', ''];
$GLOBALS['TL_LANG']['tl_module']['pbb_numberOfItems'] = ['Anzahl der Einträge', ''];
$GLOBALS['TL_LANG']['tl_module']['pbb_pinBoard'] = ['Pin Board', 'der Eintrag kann hier auf ein anderes Pin Board verschoben werden.'];
$GLOBALS['TL_LANG']['tl_module']['onlyMemberEntries'] = ['nur aktuelles Mitglied-Beiträge', 'ausschließlich die Anzeigen vom aktuell angemeldeten Mitglied anzeigen'];
$GLOBALS['TL_LANG']['tl_module']['jumpToNewEntryForm'] = ['zum "neuen Eintrag" -Formular', ''];
$GLOBALS['TL_LANG']['tl_module']['jumpToEditEntryForm'] = ['zum "Eintrag bearbeiten" -Formular', ''];
$GLOBALS['TL_LANG']['tl_module']['jumpToPinBoardEntryList'] = ['zur Nachrichtenliste', ''];
$GLOBALS['TL_LANG']['tl_module']['jumpToEntryDetails'] = ['zur Eintrag-Detailseite', ''];
$GLOBALS['TL_LANG']['tl_module']['notificationId'] = ['Benachrichtigung senden', 'Wenn ein Eintrag neu erstellt oder bearbeitet wurde soll diese Benachrichtigung (Notification-Center) gesendet werden.'];
$GLOBALS['TL_LANG']['tl_module']['noEntryForToken'] = ['Anzeigetext (kein passender Token)', 'Dieser Text wird angezeigt, wenn entweder kein Token übergeben wurde oder ein Token der zu keinem Eintrag passt.'];
$GLOBALS['TL_LANG']['tl_module']['successfullyActionForToken'] = ['Anzeigetext (erfolgreiche Aktion zu dem Token)', 'Dieser Text wird angezeigt, wenn die jeweilige Aktion erfolgreich auf den Pinnwand-Eintrag angewendet wurde.'];
