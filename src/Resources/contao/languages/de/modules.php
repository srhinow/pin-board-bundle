<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension pin-board-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Back end modules
 */
$GLOBALS['TL_LANG']['MOD']['pin_board_bundle'] = ['Kleinanzeigen', 'Die Pinnwand-Verwaltung'];
$GLOBALS['TL_LANG']['MOD']['pin_board'] = ['Pinnwände', 'Liste aller Pinnwände und deren Einträge.'];
$GLOBALS['TL_LANG']['MOD']['pin_board_setting'] = ['Einstellungen', 'Pinnwand-Einstellungen'];

/*
 * Front end modules.
 */
$GLOBALS['TL_LANG']['FMD']['pin_board'] = 'Pin Board';
$GLOBALS['TL_LANG']['FMD']['pbb_fe_pin_board_new_entry'] = ['Pin Board :: neue Nachrichten (Kopfbereich)'];
$GLOBALS['TL_LANG']['FMD']['pbb_fe_pin_board_entry_list'] = ['Pin Board :: Eintrag-Liste'];
