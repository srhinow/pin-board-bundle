<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension pin-board-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

$GLOBALS['TL_LANG']['tl_user']['lcpcaseportal_legend'] = 'Mandantenportal-Rechte';
$GLOBALS['TL_LANG']['tl_user']['teaser_legend'] = 'Teaser-Einstellungen';

$GLOBALS['TL_LANG']['tl_user']['shortName'] = [
    'Namen-Kurzform',
    'Dies wird z.B. in der Tabelle bei "meine Nachrichten" im Mandanten-Portal ausgegeben.',
];
$GLOBALS['TL_LANG']['tl_user']['contentTeaser'] = ['Content-Teaser zum Anwalt'];
$GLOBALS['TL_LANG']['tl_user']['lcp_casep'] = ['Fallrechte'];
$GLOBALS['TL_LANG']['tl_user']['lcp_case_recordp'] = ['Fall-nachrichten-Rechte'];
