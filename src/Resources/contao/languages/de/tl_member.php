<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension pin-board-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Table tl_member.
 */

$GLOBALS['TL_DCA']['tl_member']['palettes']['lcp_clients'] = '
    {personal_legend},salution,firstname,lastname,email,company;
    {groups_legend},groups;
    {account_legend},disable,start,stop;
    {activation_legend},sendLogin';

$GLOBALS['TL_DCA']['tl_member']['fields']['salution'] = [
    'exclude' => true,
    'inputType' => 'text',
    'eval' => [
        'mandatory' => false,
        'maxlength' => 255,
        'feEditable' => true,
        'feViewable' => true,
        'tl_class' => 'w50',
        'feGroup' => 'personal',
    ],
    'sql' => "varchar(125) NOT NULL default ''",
];

$GLOBALS['TL_DCA']['tl_member']['fields']['sendLogin'] = [
    'label' => &$GLOBALS['TL_LANG']['tl_member']['sendLogin'],
    'inputType' => 'checkbox',
    'eval' => ['doNotCopy' => true, 'tl_class' => 'clr'],
    'sql' => "char(1) NOT NULL default ''",
];
