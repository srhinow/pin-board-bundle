<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension pin-board-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Buttons.
 */
$GLOBALS['TL_LANG']['tl_pin_board_entry']['new'][0] = 'Neuer Eintrag';
$GLOBALS['TL_LANG']['tl_pin_board_entry']['new'][1] = 'Einen neuen Eintrag anlegen.';
$GLOBALS['TL_LANG']['tl_pin_board_entry']['edit'][0] = 'Eintrag bearbeiten';
$GLOBALS['TL_LANG']['tl_pin_board_entry']['edit'][1] = 'Eintrag ID %s bearbeiten.';
$GLOBALS['TL_LANG']['tl_pin_board_entry']['copy'][0] = 'Eintrag duplizieren';
$GLOBALS['TL_LANG']['tl_pin_board_entry']['copy'][1] = 'Eintrag ID %s duplizieren.';
$GLOBALS['TL_LANG']['tl_pin_board_entry']['delete'][0] = 'Eintrag löschen';
$GLOBALS['TL_LANG']['tl_pin_board_entry']['delete'][1] = 'Eintrag ID %s löschen.';
$GLOBALS['TL_LANG']['tl_pin_board_entry']['show'][0] = 'Eintrag anzeigen';
$GLOBALS['TL_LANG']['tl_pin_board_entry']['show'][1] = 'Eintrag anzeigen (ID: %s)';
$GLOBALS['TL_LANG']['tl_pin_board_entry']['multiEdit'][0] = 'mehrere bearbeiten';

/*
 * Fields
 */
$GLOBALS['TL_LANG']['tl_pin_board_entry']['pid'] = ['Pinwand', 'Hier können die Einträge in andere Pinwände verschoben werden.'];
$GLOBALS['TL_LANG']['tl_pin_board_entry']['tstamp'] = ['bearbeitet am'];
$GLOBALS['TL_LANG']['tl_pin_board_entry']['dateAdded'] = ['erstellt am'];
$GLOBALS['TL_LANG']['tl_pin_board_entry']['authorId'] = [
    'Nachrichten-Verfasser',
    'Der zur Nachricht gehörende Verfasser wird automatisch gesetzt.',
];
$GLOBALS['TL_LANG']['tl_pin_board_entry']['authorName'] = [
    'Name des Verfassers',
    'Der zur Nachricht gehörende Verfasser-Name wird automatisch gesetzt.',
];
$GLOBALS['TL_LANG']['tl_pin_board_entry']['sendToEmail'] = [
    'Adressat',
    'Der Nachrichten Empfänger wird hier angegeben.',
];
$GLOBALS['TL_LANG']['tl_pin_board_entry']['authorTable'] = [
    'Tabelle des Verfassers',
    'Der zur Nachricht gehörende Verfasser-Tabelle wird automatisch gesetzt. (tl_user->Anwalt, tl_member->Mandant)',
];

$GLOBALS['TL_LANG']['tl_pin_board_entry']['memberId'] = [
    'Mitglied',
    'Wählen Sie den zur Nachricht gehörenden Mandant aus.',
];

$GLOBALS['TL_LANG']['tl_pin_board_entry']['subject'] = [
    'Betreff',
    'Geben Sie hier ähnlich einer Email den Betreff der Nachricht ein.',
];
$GLOBALS['TL_LANG']['tl_pin_board_entry']['text'][0] = 'Nachricht';
$GLOBALS['TL_LANG']['tl_pin_board_entry']['upload'] = [
    'Datei-Upload',
    'Hier können Sie ein oder mehrere Dateien hochladen so das diese dann zu der Nachricht mitgesendet werden.',
];

$GLOBALS['TL_LANG']['tl_pin_board_entry']['sendBySave'] = [
    'sofort senden',
    'Aktivieren Sie dieses Kästchen, wenn Sie die Nachricht sofort versenden wollen.',
];
$GLOBALS['TL_LANG']['tl_pin_board_entry']['readyToSend'] = [
    'Versand planen',
    'Aktivieren Sie dieses Kästchen, wenn Sie den Nachrichtenversand planen wollen.
    Sie können dann eine Uhrzeit festlegen.',
];
$GLOBALS['TL_LANG']['tl_pin_board_entry']['sendByDate'] = ['Versanddatum und -zeit'];
$GLOBALS['TL_LANG']['tl_pin_board_entry']['published'] = ['veröffentlicht'];
$GLOBALS['TL_LANG']['tl_pin_board_entry']['dateExpired'] = ['verfügbar bis', 'Wenn dieses Datum in der Vergangenheit liegt, wird dieser Eintrag durch den Cronjob automatisch gelöscht.'];

$GLOBALS['TL_LANG']['tl_pin_board_entry']['upload_button_text'] = 'Datei(en) hochladen';

/*
 * Legends
 */
$GLOBALS['TL_LANG']['tl_pin_board_entry']['main_legend'] = 'Haupt-Einstellungen';
$GLOBALS['TL_LANG']['tl_pin_board_entry']['message_legend'] = 'Nachrichten-Einstellungen';
$GLOBALS['TL_LANG']['tl_pin_board_entry']['protected_legend'] = 'Einstellungen vom geschützten Bereich';
$GLOBALS['TL_LANG']['tl_pin_board_entry']['upload_legend'] = 'Datei-Einstellungen';
$GLOBALS['TL_LANG']['tl_pin_board_entry']['extend_legend'] = 'weitere Einstellungen';

/*
 * Meldungen
 */
$GLOBALS['TL_LANG']['tl_pin_board_entry']['delete_files_success']
    = 'Es wurden nicht verwendete Dateien aus dem Aktenordner gelöscht.';
$GLOBALS['TL_LANG']['tl_pin_board_entry']['no_entries_msg'] = 'Es sind noch keine Nachrichten vorhanden.';
$GLOBALS['TL_LANG']['tl_pin_board_entry']['save_new_entries_msg'] = 'Die Nachricht wurde angelegt.';
