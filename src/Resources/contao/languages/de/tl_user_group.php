<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension pin-board-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

$GLOBALS['TL_LANG']['tl_user_group']['lcpcaseportal_legend'] = 'Mandantenportal-Rechte';

$GLOBALS['TL_LANG']['tl_user_group']['lcp_casep'] = ['Fallrechte'];
$GLOBALS['TL_LANG']['tl_user_group']['lcp_case_recordp'] = ['Fall-Nachrichten-Rechte'];
