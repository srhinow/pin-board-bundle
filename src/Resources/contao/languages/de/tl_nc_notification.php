<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension pin-board-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

$GLOBALS['TL_LANG']['tl_nc_notification']['type']['pin_board'] = 'Pin-Board';
$GLOBALS['TL_LANG']['tl_nc_notification']['type']['pbb_send_new_entry'] = ['Pin-Board neuer Eintrag', 'Wenn der Eintrag öffentlich angelegt wurde, können hiermit die Aktivierungs-URl,Bearbeitungs-Url und Löschen-URL gesendet werden.'];
