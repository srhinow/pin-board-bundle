<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension pin-board-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

$GLOBALS['PINBOARD_SETTINGS']['ID'] = 1;
$GLOBALS['PINBOARD_PUBLIC_FOLDER'] = 'bundles/srhinowpinboard';
$GLOBALS['PINBOARD_ENTRY_FOLDER'] = 'pin_board_entries';

/*
 * -------------------------------------------------------------------------
 * BACK END MODULES
 * -------------------------------------------------------------------------
 */
if ('BE' === TL_MODE) {
    $GLOBALS['TL_CSS'][] = $GLOBALS['PINBOARD_PUBLIC_FOLDER'].'/css/be.css|static';
}

array_insert($GLOBALS['BE_MOD'], 1, ['pin_board_bundle' => []]);

$GLOBALS['BE_MOD']['pin_board_bundle']['pin_board'] = [
    'tables' => ['tl_pin_board', 'tl_pin_board_entry'],
];

$GLOBALS['BE_MOD']['pin_board_bundle']['pin_board_setting'] = [
    'tables' => ['tl_pin_board_setting'],
    'callback' => 'Srhinow\PinBoardBundle\Module\Backend\ModulePinBoardSettings',
];

/*
 * -------------------------------------------------------------------------
 * Front END MODULES
 * -------------------------------------------------------------------------
 */
array_insert(
    $GLOBALS['FE_MOD'],
    2,
    [
        'pin_board' => [
            'pbb_fe_pin_board_entry_form' => 'Srhinow\PinBoardBundle\Module\Frontend\ModulePinBoardEntryForm',
            'pbb_fe_pin_board_entry_details' => 'Srhinow\PinBoardBundle\Module\Frontend\ModulePinBoardEntryDetails',
            'pbb_fe_entry_list' => 'Srhinow\PinBoardBundle\Module\Frontend\ModulePinBoardEntryList',
            'pbb_activate_entry' => 'Srhinow\PinBoardBundle\Module\Frontend\ModulePinBoardActivateEntry',
            'pbb_delete_entry' => 'Srhinow\PinBoardBundle\Module\Frontend\ModulePinBoardDeleteEntry',
        ],
    ]
);

/*
 * -------------------------------------------------------------------------
 * MODELS
 * -------------------------------------------------------------------------
 */
$GLOBALS['TL_MODELS']['tl_pin_board'] = Srhinow\PinBoardBundle\Model\PinBoardModel::class;
$GLOBALS['TL_MODELS']['tl_pin_board_entry'] = Srhinow\PinBoardBundle\Model\PinBoardEntryModel::class;
$GLOBALS['TL_MODELS']['tl_pin_board_setting'] = Srhinow\PinBoardBundle\Model\PinBoardSettingModel::class;

/*
 * -------------------------------------------------------------------------
 * Permissions are access settings for user and groups (fields in tl_user and tl_user_group)
 * -------------------------------------------------------------------------
 */
$GLOBALS['TL_PERMISSIONS'][] = 'pbb_pin_boardp';
$GLOBALS['TL_PERMISSIONS'][] = 'pbb_pin_board_entryp';

/*
 * -------------------------------------------------------------------------
 * Bereitgesteller Notification-Typ und dafuer verfuegbare Tokens
 * -------------------------------------------------------------------------
 */
$GLOBALS['NOTIFICATION_CENTER']['NOTIFICATION_TYPE']['pin_board'] = [
    // Type
    'pbb_send_new_entry' => [
        // Field in tl_nc_language
        'email_sender_address' => ['entry_owner_email'],
        'recipients' => ['entry_owner_email'],
        'email_replyTo' => ['entry_owner_email'],
        'email_recipient_cc' => ['entry_owner_email'],
        'email_recipient_bcc' => ['entry_owner_email'],
        'email_subject' => ['email_subject'],
        'email_text' => [
            'entry_owner_email',
            'activate_token', 'edit_token', 'delete_token',
            'activate_link', 'edit_link', 'delete_link',
        ],
        'email_html' => [
            'entry_owner_email',
            'activate_token', 'edit_token', 'delete_token',
            'activate_link', 'edit_link', 'delete_link',
        ],
    ],
];
