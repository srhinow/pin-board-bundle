# Pin-Board-Bundle

Eine kleine Contao-Erweiterung um  Pinnwände bzw Kleinanzeigen zu verwalten 

## Backend

- mehrere Pin-boards möglich
- Backend-Verwaltung der Pinwände und der dazugehörenden Einträge
- FineUpload optional möglich
- Text-Einstellungen für "vor Upload-Feld" und "keine Einträge" in der jeweiligen Modul-Einstellung möglich
- gewohnte Contao-Paginierung integriert

## Frontend

- öffentliche / oder Member-geschützte Listen/Eintragsformulare möglich
- Filter nach Suchbegriff und Datum ab/aufsteigend möglich
- Liste für alle zum Board gehörenden Einträge oder nur vom aktuell angemeldeten Member
- optionale FineUpload (terminal42) Drag&Drop-Funktion für Anhänge bzw Dateien zur Anzeige
- edit/delete Funktionen für die Anzeigen die zum aktuell angemeldeten Frontend-User gehören.

### ab 1.2.0
- erstellen von Einträgen im öffentlichen Bereich mit Email-Adresse und Token für das Notification-Center die verwendet werden können damit der Ersteller des Pinnwand-Eintrages diesen durch klick auf den Link aktivieren muss, den Eintrag editieren oder löschen kann: 
  - ##entry_owner_email##, ##activate_token##, ##edit_token##, ##delete_token##,
            ##activate_link##, ##edit_link##, ##delete_link##
- Wenn in den Kleinanzeigen-Einstellungen / Cron-Einstellungen einen Wert > 0 eingegeben wird. Dann werden die Einträge dementsprechend in Tage automatisch wieder gelöscht.
- beim entfernen eines Eintrages (egal ob link,Backend oder Cron) wird auch der zum Eintrag gehörende Upload-Ordner gelöscht. 